#!/usr/bin/python

from init import *
import ath_hst
import glob
import os
from optparse import OptionParser

def plot_mdothst(ax,star,**kwargs):
	x=np.array(star['age'])*t0
	y=np.array(star['mdot'])*mdot0
	ax.plot(x,y,kwargs)

def plot_mhst(ax,star,**kwargs):
	x=np.array(star['age'])*t0
	y=np.array(star['mass'])*mdot0*t0
	ax.plot(x,y,kwargs)

parser = OptionParser()
parser.add_option('-b','--base-directory', dest='base_directory',
                  help='base directory for data',
                  default='/scr1/cgkim/Research/athena_StarParticle/bin/')
parser.add_option('-d','--directory',dest='directory',
                  help='working directory',action='append')
parser.add_option('-i','--id',dest='id',
                  help='id of dataset',action='append')
parser.add_option('-s','--spid',dest='spid',
                  help='id of starparticle',type='int')
parser.add_option('-o','--output',dest='out_fname',
                  help='output file name')
(options, args) = parser.parse_args()

print options
print len(options.directory)
print len(options.id)
files=[]
ncomp = len(options.directory)+len(options.id)
if ncomp == 1: style='-'
else: style = ['b-','r-','g-']

fig=plt.figure(figsize=(8,8))
ax1=fig.add_subplot(211)
ax2=fig.add_subplot(212)

t0=pc/1.e5/1.e6/yr # in units of Myr
mdot0=1.4*mp*pc**3/msun/t0 # in units of M_sun/yr

n=0
for dir in options.directory:
	for id in options.id:
		fsearch = options.base_directory 
		if dir != None: fsearch = fsearch+dir
		if id != None: fsearch = fsearch+id
		if options.spid == None: 
			spid = ''
			fsearch = fsearch+'.*.star'
		else: 
			spid = '.%4.4d.star' % options.spid
			fsearch = fsearch+spid
		fall=glob.glob(fsearch)
		fall.sort()

		print n
		for file in fall:
			star=ath_hst.read(file,sortable_key=True)
			keys=star.keys()
			keys.sort()
			x=np.array(star[keys[8]])*t0
			y=np.array(star[keys[9]])*mdot0/1.e6
			ax1.plot(x,y,style[n],label=os.path.basename(file))
			x=np.array(star[keys[8]])*t0
			y=np.array(star[keys[1]])*mdot0*t0
			ax2.plot(x,y,style[n])

		if len(fall) != 0: n=n+1

if options.out_fname == None: out_fname='png/'+id+spid+'_acchst.png'
else: out_fname=options.out_fname


#xlim=ax1.get_xlim()
if ncomp == 1: ax1.legend(loc=0,frameon=False)
ax2.set_xlabel('t [Myr]')
ax1.set_ylabel(r'$\dot{M} [{\rm M_{\odot}/yr}]$')
ax2.set_ylabel(r'$M [{\rm M_{\odot}}]$')
ax1.set_yscale('log')
ax2.set_yscale('log')
plt.setp((ax1,ax2),'xlim',(0,10))
ax1.set_ylim(1.e-6,1.e-2)
ax2.set_ylim(1.e2,1.e5)
ax2.axhline(1.15e3,ls=':')
#mystyle()
plt.tight_layout()
fig.savefig(out_fname,dpi=100)
