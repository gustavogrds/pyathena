import pyathena as pa
import pandas as pd
import glob
import os
import argparse
import string
from ath_hst import test_pickle

def main(**kwargs):
  dir=kwargs['base_directory']+kwargs['directory']
  id=kwargs['id']
  if kwargs['serial']: 
    file = glob.glob(dir+id+".????.whole.zprof")
  else:
    file = glob.glob(dir+"id0/"+id+".????.whole.zprof")
  file.sort()
  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
  else:
    start = 0
    end = len(file)
    fskip = 1

  print start,end,fskip
  file=file[start:end:fskip]

  if not os.path.isdir(dir+'pickles/'): os.mkdir(dir+'pickles/')

  time=[]
  zp_panels={}
  zp=pa.AthenaZprof(file[0])
  for p in zp.plist:
    zp_panels[p]={}

  for f in file:
    print 'Stitching...',f
    zp=pa.AthenaZprof(f)

  for f in file:
    zp=pa.AthenaZprof(f)
    time.append(zp.time)
    for p in zp.plist:
      zp_panels[p][f]=zp.read(phase=p)

  for p in zp.plist:
    pn=pd.Panel.from_dict(zp_panels[p],orient='minor')
    pn.minor_axis=time
    fname=zp.path+'pickles/'+string.join([zp.id,p,zp.ext],'.')
    pn.to_pickle(fname+'.p')
    print 'Output: ',fname+'.p'

  for f in file:
    print 'Cleaning... ',f
    zp=pa.AthenaZprof(f,stitch=False,clean=True)
  

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-s','--serial',action='store_true',help='serial mode')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  args = parser.parse_args()
  main(**vars(args))
