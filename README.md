# This is my own python scritps for Athena

from pyathena import *

ds=AthenaDataSet(*filename*)

### AthenaDataSet class

 * ds.domain (dictionary containing information for domain)
 * ds.grids (list of dictionaries containing information for each grid (cores))
 * ds.domain['field_map'] (dictionary containing information for file I/O)

 * ds.set_grid_data(*grid*,*field*) (setting data for "field" into each grid dictionary)
 * *data*=ds.read_all_data(*field*) (return the "field" data for whole domain; array[nx3,nx2,nx1])

[Example Notebook](http://nbviewer.ipython.org/url/www.astro.princeton.edu/~cgkim/notebook/pyathena_example.ipynb)