from pyathena import *
import glob
import os
import argparse
import cPickle as pickle

# astropy
import astropy.constants as c
import astropy.units as u

def main(**kwargs):
  id=kwargs['id']
  if kwargs['directory'] == '': kwargs['directory'] = id+'/'
  dir=kwargs['base_directory']+kwargs['directory']
  vtkfile = glob.glob(dir+"id0/"+id+".????.vtk")
  if len(vtkfile) == 0 :
    vtkfile = glob.glob(dir+id+".????.vtk")
  vtkfile.sort()

  dspickle=dir+'pickles/'+id+'.0000.ds.p'
  ds = pickle.load(open(dspickle,'rb'))

  fm = ds.domain['field_map']
  grids=ds.grids

  vprof={}
  for vf in vtkfile:
    ds = AthenaDataSet(vf,grids=grids)
    time = ds.domain['time']
    print 'Reading: ',vf,time

    slicepickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'surf',1)
    turbpickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'Bturb',1)
    denpickle=dir+'pickles/'+'%s.%s.%s.p' % (ds.id,ds.step,'den')
    slc = pickle.load(open(slicepickle,'rb'))
    if 'magnetic_field' in ds.field_list: bturb = pickle.load(open(turbpickle,'rb'))
    else: bturb={}
    if os.path.isfile(denpickle): den = pickle.load(open(denpickle,'rb'))
    else: den ={}

# initialization

    if len(vprof) == 0:
      for field in slc:
	vprof[field]=[] 
      for field in bturb:
	vprof[field]=[] 
      for field in den:
	vprof[field]=[] 
      vprof['time']=[]

    for field in slc:
      vprof[field].append(slc[field].data.mean(axis=1))
    for field in bturb:
      vprof[field].append(bturb[field].mean(axis=1))
    for field in den:
      vprof[field].append(den[field])
    vprof['time'].append(time)

  le=ds.domain['left_edge']
  re=ds.domain['right_edge']
  dx=ds.domain['dx']

  for field in vprof: vprof[field]=np.array(vprof[field])
  vprof['axis']=np.arange(le[2],re[2],dx[2])+dx[2]*0.5

  vprofpickle=dir+'%s.%s.p' % (ds.id,'vprof')
  pickle.dump(vprof,open(vprofpickle,'wb'),pickle.HIGHEST_PROTOCOL)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  args = parser.parse_args()
  main(**vars(args))
