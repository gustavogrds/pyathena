import ath_hst
import glob
import os
import argparse

import cPickle as pickle

# matplotlib
import matplotlib
from mpl_toolkits.axes_grid1 import AxesGrid,ImageGrid
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import rc

# numpy
import numpy as np

# astropy
import astropy.constants as c
import astropy.units as u

# utility
import myutilities as myutil

def texteffect():
  try:
    from matplotlib.patheffects import withStroke
    myeffect = withStroke(foreground="w", linewidth=3)
    kwargs = dict(path_effects=[myeffect], fontsize=12)
  except ImportError:
    kwargs = dict(fontsize=12)
  return kwargs

class hstdata(object):
  def __init__(self,file):
    self.hst=ath_hst.read_w_pandas(file)
    self.vol=ath_hst.get_volume(file)*c.pc**3
    self.lunit = c.pc
    self.vunit = 1.0*u.km/u.s
    self.dunit = 1.4*c.m_p/u.cm**3
    self.tunit = (self.lunit/self.vunit).to('Myr')
    self.Punit = (self.dunit*self.vunit**2/c.k_B).to('K/cm^3')
    self.Msun = (self.dunit*self.lunit**3).to('Msun')

    self.Nz = 1024#2.0/self.hst.Vmid_2p[0]
    self.Lz = 2.0*self.Nz*c.pc
    self.area = self.vol/self.Lz
    self.time = self.hst.time*self.tunit
    self.dt = self.hst.dt*self.tunit
    self.surf = self.hst.mass*(self.dunit*self.vol/self.area).to('Msun/pc^2')
    self.surf_sp = self.hst.msp*(self.dunit*self.vol/self.area).to('Msun/pc^2')

    self.v1 = np.sqrt(2.0*self.hst.x1KE/self.hst.mass)*self.vunit
    self.v2 = np.sqrt(2.0*self.hst.x2dke/self.hst.mass)*self.vunit
    self.v3 = np.sqrt(2.0*self.hst.x3KE/self.hst.mass)*self.vunit

    self.v1_2p = np.sqrt(2.0*self.hst.x1KE_2p/self.hst.mass)*self.vunit
    self.v2_2p = np.sqrt(2.0*self.hst.x2KE_2p/self.hst.mass)*self.vunit
    self.v3_2p = np.sqrt(2.0*self.hst.x3KE_2p/self.hst.mass)*self.vunit

    self.scaleH = np.sqrt(self.hst.H2/self.hst.mass)
    self.scaleHh = np.sqrt(self.hst.H2h/self.hst.Mh)
    self.scaleHw = np.sqrt(self.hst.H2w/self.hst.Mw)
    self.scaleHu = np.sqrt(self.hst.H2u/self.hst.Mu)
    self.scaleHc = np.sqrt(self.hst.H2c/self.hst.Mc)

    self.fmh = self.hst.Mh/self.hst.mass
    self.fmw = self.hst.Mw/self.hst.mass
    self.fmu = self.hst.Mu/self.hst.mass
    self.fmc = self.hst.Mc/self.hst.mass

    self.fvh = self.hst.Vh
    self.fvw = self.hst.Vw
    self.fvu = self.hst.Vu
    self.fvc = self.hst.Vc

    self.Pth = self.hst.Pth*self.Punit
    self.Pturb = self.hst.Pturb*self.Punit
    self.Pth_2p = self.hst.Pth_2p/self.hst.Vmid_2p*self.Punit
    self.Pturb_2p = self.hst.Pturb_2p/self.hst.Vmid_2p*self.Punit

    if 'F3c' in self.hst:
      self.funit=self.Msun/self.tunit
      self.zfluxc=self.hst.F3c*self.funit
      self.zfluxu=self.hst.F3u*self.funit
      self.zfluxw=self.hst.F3w*self.funit
      self.zfluxh=self.hst.F3h*self.funit
      self.zflux2p=self.zfluxc+self.zfluxu+self.zfluxw
      self.b10_2p=self.zflux2p/self.hst.sfr10
      self.b10_h=self.zfluxh/self.hst.sfr10
      self.b40_2p=self.zflux2p/self.hst.sfr40
      self.b40_h=self.zfluxh/self.hst.sfr40
      self.b100_2p=self.zflux2p/self.hst.sfr100
      self.b100_h=self.zfluxh/self.hst.sfr100

    self.basename=os.path.splitext(file)[0]

  def read_star(self):
    self.stars=[]
    self.starfiles=glob.glob(self.basename+'.????.star')
    self.starfiles.sort()
    for sf in self.starfiles:
      self.stars.append(ath_hst.read_w_pandas(sf))

  def read_sn(self):
    self.sn=ath_hst.read_w_pandas(self.basename+'.sn')

def plot(self,axes):
    for akey in axes:
      if akey is 'dt':
        axes[akey].plot(self.time,self.dt)
      elif akey is 'velocity':
        #l,=axes[akey].plot(self.time,self.v1,ls=':')
        #axes[akey].plot(self.time,self.v2,ls='--',color=l.get_color())
        #axes[akey].plot(self.time,self.v3,ls='-',color=l.get_color())
        l,=axes[akey].plot(self.time,self.v1_2p,ls=':')
        axes[akey].plot(self.time,self.v2_2p,ls='--',color=l.get_color())
        axes[akey].plot(self.time,self.v3_2p,ls='-',color=l.get_color())
      elif akey is 'mass':
        l,=axes[akey].plot(self.time,self.surf,ls=':')
        axes[akey].plot(self.time,self.surf_sp,ls='--',color=l.get_color())
        axes[akey].plot(self.time,self.surf+self.surf_sp,ls='-',color=l.get_color())
      elif akey is 'sfr':
        l,=axes[akey].plot(self.time,self.hst.sfr10,ls='-')
        axes[akey].plot(self.time,self.hst.sfr40,ls=':',color=l.get_color())
        axes[akey].plot(self.time,self.hst.sfr100,ls='--',color=l.get_color())
      elif akey is 'HeatRatio':
        l,=axes[akey].plot(self.time,self.hst.sfr10/2.5e-3,ls=':')
        axes[akey].plot(self.time,self.hst.heat_ratio,color=l.get_color())
      elif akey is 'scaleH':
        l,=axes[akey].plot(self.time,self.scaleH)
        axes[akey].plot(self.time,self.scaleHc,'--',color=l.get_color())
        axes[akey].plot(self.time,self.scaleHu,':',color=l.get_color())
      elif akey is 'MassFraction':
        l,=axes[akey].plot(self.time,self.fmh,ls='-')
        axes[akey].plot(self.time,self.fmw,ls=':',color=l.get_color())
        axes[akey].plot(self.time,self.fmu,ls='--',color=l.get_color())
        axes[akey].plot(self.time,self.fmc,ls='-.',color=l.get_color())
      elif akey is 'VolumeFraction':
        l,=axes[akey].plot(self.time,self.fvh,ls='-')
        axes[akey].plot(self.time,self.fvw,ls=':',color=l.get_color())
        axes[akey].plot(self.time,self.fvu,ls='--',color=l.get_color())
        axes[akey].plot(self.time,self.fvc,ls='-.',color=l.get_color())
      elif akey is 'pressure':
        #l,=axes[akey].plot(self.time,self.Pth,ls='-')
        axes[akey].plot(self.time,self.Pth_2p,ls='-')
        #l,=axes[akey].plot(self.time,self.Pturb,ls='-')#,color=l.get_color())
        axes[akey].plot(self.time,self.Pturb_2p,ls='-')
      elif akey is 'Pratio':
        l,=axes[akey].plot(self.time,self.Pturb_2p/self.Pth_2p,ls='-')
        axes[akey].plot(self.time,self.Pturb/self.Pth,ls=':',color=l.get_color())

def plot_single(self,axes):
    for akey in axes:
      if akey is 'dt':
        axes[akey].plot(self.time,self.dt)
      elif akey is 'velocity':
        axes[akey].plot(self.time,self.v1,ls=':',label='v1')
        axes[akey].plot(self.time,self.v2,ls='--',label='v2')
        axes[akey].plot(self.time,self.v3,ls='-',label='v3')
        axes[akey].legend(loc=0)
      elif akey is 'mass':
        axes[akey].plot(self.time,self.surf+self.surf_sp,ls='-',label='total')
        axes[akey].plot(self.time,self.surf,ls=':',label='gas')
        axes[akey].plot(self.time,self.surf_sp,ls='--',label='star')
        axes[akey].legend(loc=0)
      elif akey is 'sfr':
        axes[akey].plot(self.time,self.hst.sfr10,ls='-',label=r'$\Sigma_{\rm sfr,10}$')
        axes[akey].plot(self.time,self.hst.sfr40,ls=':',label=r'$\Sigma_{\rm sfr,40}$')
        axes[akey].plot(self.time,self.hst.sfr100,ls='--',label=r'$\Sigma_{\rm sfr,100}$')
        axes[akey].legend(loc=0)
      elif akey is 'HeatRatio':
        axes[akey].plot(self.time,self.hst.heat_ratio,label=r'$\Gamma/\Gamma_0$')
        axes[akey].plot(self.time,self.hst.sfr10/2.5e-3,label=r'$\Sigma_{\rm sfr,10}/\Sigma_{\rm sfr,0}$')
        axes[akey].legend(loc=0)
      elif akey is 'scaleH':
        axes[akey].plot(self.time,self.scaleHc,label='cold')
        axes[akey].plot(self.time,self.scaleHw,label='warm')
        axes[akey].plot(self.time,self.scaleHh,label='hot')
        axes[akey].plot(self.time,self.scaleHu,label='unstable')
        axes[akey].plot(self.time,self.scaleH,'k',label='total')
        axes[akey].legend(loc=0)
      elif akey is 'MassFraction':
        axes[akey].plot(self.time,self.fmc,label='cold')
        axes[akey].plot(self.time,self.fmw,label='warm')
        axes[akey].plot(self.time,self.fmh,label='hot')
        axes[akey].plot(self.time,self.fmu,label='unstable')
        axes[akey].legend(loc=0)
      elif akey is 'VolumeFraction':
        axes[akey].plot(self.time,self.fvc,label='cold')
        axes[akey].plot(self.time,self.fvw,label='warm')
        axes[akey].plot(self.time,self.fvh,label='hot')
        axes[akey].plot(self.time,self.fvu,label='unstable')
        axes[akey].legend(loc=0)
      elif akey is 'pressure':
        #l,=axes[akey].plot(self.time,self.Pth,ls='-')
        axes[akey].plot(self.time,self.Pth_2p,label='Pth')
        #l,=axes[akey].plot(self.time,self.Pturb,ls='-')#,color=l.get_color())
        axes[akey].plot(self.time,self.Pturb_2p,label='Pturb')
        axes[akey].legend(loc=0)
      elif akey is 'Pratio':
        axes[akey].plot(self.time,self.Pturb_2p/self.Pth_2p,label='2p')
        axes[akey].plot(self.time,self.Pturb/self.Pth,label='all')
        axes[akey].legend(loc=0)


def set_figure(fig=plt.gcf(),fields=['dt','mass','velocity',\
            'scaleH','sfr','HeatRatio',\
            'MassFraction','VolumeFraction','Pratio']):
    axes={}
    nkeys=len(fields)
    nrow=nkeys
    ncol=1
    if nkeys > 5 and nkeys<=8:
      ncol=2
      nrow=nkeys/ncol
    elif nkeys>8:
      ncol=3
      nrow=nkeys/ncol


    plt.clf()
    for iax,field in enumerate(fields):
      ax=plt.subplot(nrow,ncol,iax+1)
      axes[field]=ax
      ax.set_ylabel(field)
    return axes

def test1(file='/tiger/scratch/gpfs/changgoo/ST256x1024/id0/ST256x1024.hst'):
  hdata=hstdata(file)
  axes=set_figure()
  plot_single(hdata,axes)
  plt.tight_layout()

  return axes

def test2():
  files=[]
#  files.append('/tiger/scratch/gpfs/changgoo/ST256x1024/id0/ST256x1024.hst')
  files.append('/tiger/scratch/gpfs/changgoo/ST128x1024/id0/ST128x1024.hst')
  files.append('/tiger/scratch/gpfs/changgoo/ST128x1024/id0/ST128x1024r1.hst')
  files.append('/tiger/scratch/gpfs/changgoo/ST128x1024/id0/ST128x1024r2.hst')
  axes=set_figure()
  for file in files:
    hdata=hstdata(file)
    plot(hdata,axes)
  plt.tight_layout()

  return axes
