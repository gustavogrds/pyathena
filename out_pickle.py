import pyathena as pa
import glob
import os
import argparse
import cPickle as pickle

import numpy as np

# astropy
import astropy.constants as c
import astropy.units as u

# helicity
from derivative import helicity

def construct3d(array,shape,axis=0):
  if array.shape[0] != shape[axis]:
    print "Error: input array dimension (%d) should match with output dimension (%d) of axis %d" % (array.shape[0], shape[axis], axis)
    return -1
  else:
    if axis == 0:
      array_3d = np.tile(array.reshape(shape[0],1,1),(1,shape[1],shape[2]))
    elif axis == 1:
      array_3d = np.tile(array.reshape(1,shape[1],1),(shape[0],1,shape[2]))
    elif axis == 2:
      array_3d = np.tile(array.reshape(1,1,shape[2]),(shape[0],shape[1],shape[2]))
    return array_3d


def dynamo(ds):
  B1 = ds.read_all_data('magnetic_field1')
  B2 = ds.read_all_data('magnetic_field2')
  B3 = ds.read_all_data('magnetic_field3')

  v1 = ds.read_all_data('velocity1')
  v2 = ds.read_all_data('velocity2')
  v3 = ds.read_all_data('velocity3')

  B1mean = (B1.mean(axis=1)).mean(axis=1)
  B2mean = (B2.mean(axis=1)).mean(axis=1)
  B3mean = (B3.mean(axis=1)).mean(axis=1)

  B1turb = B1 - construct3d(B1mean,B1.shape)
  B2turb = B2 - construct3d(B2mean,B2.shape)
  B3turb = B3 - construct3d(B3mean,B3.shape)

  emf1=v2*B3turb-v3*B2turb
  emf2=v3*B1turb-v1*B3turb
  emf3=v1*B2turb-v2*B1turb

  emf1mean=emf1.mean(axis=1).mean(axis=1)
  emf2mean=emf2.mean(axis=1).mean(axis=1)
  emf3mean=emf3.mean(axis=1).mean(axis=1)

  alpha11=emf1mean/B1mean
  alpha12=emf1mean/B2mean
  alpha13=emf1mean/B3mean
  alpha21=emf2mean/B1mean
  alpha22=emf2mean/B2mean
  alpha23=emf2mean/B3mean
  alpha31=emf3mean/B1mean
  alpha32=emf3mean/B2mean
  alpha33=emf3mean/B3mean

  emf=[emf1mean,emf2mean,emf3mean]
  alpha=[[alpha11,alpha12,alpha13],[alpha22,alpha22,alpha23],[alpha31,alpha32,alpha33]]

  fname='%s/pickles/%s.%s.%s.p' % (ds.dir,ds.id,ds.step,'emf')
  pickle.dump({'emf':emf,'alpha':alpha,open(fname,'wb'),pickle.HIGHEST_PROTOCOL)

def turb_bfield(ds):
  B1 = ds.read_all_data('magnetic_field1')
  B2 = ds.read_all_data('magnetic_field2')
  B3 = ds.read_all_data('magnetic_field3')

  v1 = ds.read_all_data('velocity1')
  v2 = ds.read_all_data('velocity2')
  v3 = ds.read_all_data('velocity3')
  ind = (v3 > 0)
  nv3 = np.zeros(v3.shape)
  nv3[-ind] = v3[-ind]
  pv3 = np.zeros(v3.shape)
  pv3[ind] = v3[ind]

  B1mean = (B1.mean(axis=1)).mean(axis=1)
  B2mean = (B2.mean(axis=1)).mean(axis=1)
  B3mean = (B3.mean(axis=1)).mean(axis=1)

  B1turb = B1 - construct3d(B1mean,B1.shape)
  B2turb = B2 - construct3d(B2mean,B2.shape)
  B3turb = B3 - construct3d(B3mean,B3.shape)

  s=B1.shape
  print s
  for axis in range(3):
    Bturb = {}
    Bturb['turb_magnetic_energy1']=(B1turb**2/(8*np.pi)).mean(axis=axis)
    Bturb['turb_magnetic_energy2']=(B2turb**2/(8*np.pi)).mean(axis=axis)
    Bturb['turb_magnetic_energy3']=(B3turb**2/(8*np.pi)).mean(axis=axis)
    Bturb['turb_magnetic_pressure']=((B1turb**2+B2turb**2+B3turb**2)/(8*np.pi)).mean(axis=axis)
    Bturb['turb_magnetic_stress']=((B1turb*B2turb)/(4*np.pi)).mean(axis=axis)

    Bturb['turb_magnetic_zfluxp1']=(B1turb*pv3).mean(axis=axis)
    Bturb['turb_magnetic_zfluxp2']=(B2turb*pv3).mean(axis=axis)
    Bturb['turb_magnetic_zfluxp3']=(B3turb*pv3).mean(axis=axis)

    Bturb['turb_magnetic_zfluxn1']=(B1turb*nv3).mean(axis=axis)
    Bturb['turb_magnetic_zfluxn2']=(B2turb*nv3).mean(axis=axis)
    Bturb['turb_magnetic_zfluxn3']=(B3turb*nv3).mean(axis=axis)

    Bturb['magnetic_zfluxp1']=(B1*pv3).mean(axis=axis)
    Bturb['magnetic_zfluxp2']=(B2*pv3).mean(axis=axis)
    Bturb['magnetic_zfluxp3']=(B3*pv3).mean(axis=axis)

    Bturb['magnetic_zfluxn1']=(B1*nv3).mean(axis=axis)
    Bturb['magnetic_zfluxn2']=(B2*nv3).mean(axis=axis)
    Bturb['magnetic_zfluxn3']=(B3*nv3).mean(axis=axis)

    Bpickle='%s/pickles/%s.%s.%s.%1d.p' % (ds.dir,ds.id,ds.step,'Bturb',axis)
    pickle.dump(Bturb,open(Bpickle,'wb'),pickle.HIGHEST_PROTOCOL)

    Bturb = {}
     
    if axis == 0:
      Bturb1=B1turb[s[axis]/2+1,:,:]
      Bturb2=B2turb[s[axis]/2+1,:,:]
      Bturb3=B3turb[s[axis]/2+1,:,:]
    if axis == 1:
      Bturb1=B1turb[:,s[axis]/2+1,:]
      Bturb2=B2turb[:,s[axis]/2+1,:]
      Bturb3=B3turb[:,s[axis]/2+1,:]
    if axis == 2:
      Bturb1=B1turb[:,:,s[axis]/2+1]
      Bturb2=B2turb[:,:,s[axis]/2+1]
      Bturb3=B3turb[:,:,s[axis]/2+1]
    Bturb['turb_magnetic_energy1']=Bturb1**2/(8*np.pi)
    Bturb['turb_magnetic_energy2']=Bturb2**2/(8*np.pi)
    Bturb['turb_magnetic_energy3']=Bturb3**2/(8*np.pi)
    Bturb['turb_magnetic_pressure']=(Bturb1**2+Bturb2**2+Bturb3**2)/(8*np.pi)
    Bturb['turb_magnetic_stress']=(Bturb1*Bturb2)/(4*np.pi)
    Bpickle='%s/pickles/%s.%s.%s.%1d.p' % (ds.dir,ds.id,ds.step,'Bturbs',axis)
    pickle.dump(Bturb,open(Bpickle,'wb'),pickle.HIGHEST_PROTOCOL)

def turb_helicity(ds):
  hpickle='%s/pickles/%s.%s.%s.p' % (ds.dir,ds.id,ds.step,'helicity')

  dm=ds.domain
  le=dm['left_edge']+0.5*dm['dx']
  re=dm['right_edge']-0.5*dm['dx']
  Nx=dm['Nx']
  x=np.linspace(le[0],re[0],Nx[0])
  y=np.linspace(le[1],re[1],Nx[1])
  z=np.linspace(le[2],re[2],Nx[2])

  h={}
  v1 = ds.read_all_data('velocity1')
  v2 = ds.read_all_data('velocity2')
  v3 = ds.read_all_data('velocity3')
  HV=helicity(v1,v2,v3,x,y,z)
  h['turb_kinetic']=HV

  if 'magnetic_field' in ds.field_list:
    B1 = ds.read_all_data('magnetic_field1')
    B2 = ds.read_all_data('magnetic_field2')
    B3 = ds.read_all_data('magnetic_field3')
 
    B1mean = (B1.mean(axis=1)).mean(axis=1)
    B2mean = (B2.mean(axis=1)).mean(axis=1)
    B3mean = (B3.mean(axis=1)).mean(axis=1)
 
    B1turb = B1 - construct3d(B1mean,B1.shape)
    B2turb = B2 - construct3d(B2mean,B2.shape)
    B3turb = B3 - construct3d(B3mean,B3.shape)
 
    HB=helicity(B1,B2,B3,x,y,z)
    HBturb=helicity(B1turb,B2turb,B3turb,x,y,z)
    h['total_magnetic']=HB
    h['turb_magnetic']=HBturb
  pickle.dump(h,open(hpickle,'wb'),pickle.HIGHEST_PROTOCOL)


def density_decomp(ds):
  dpickle='%s/pickles/%s.%s.%s.p' % (ds.dir,ds.id,ds.step,'den')

  Tcold=184.
  Twarm=5050.
  Thot=2.e4
  Thot2=5.e5

  Tlist = [0,Tcold,Twarm,Thot,Thot2,np.inf]
  den = ds.read_all_data('density')
  temp = ds.read_all_data('temperature')

  dcomp={}
  for T1,T2,comp in zip(Tlist[:-1],Tlist[1:],['cnm','unm','wnm','hot1','hot2']):
	idx = (temp > T1) * (temp <= T2)
	dcmp = np.zeros(den.shape)
  	dcmp[idx]=den[idx]
        dcomp[comp]=dcmp.mean(axis=1).mean(axis=1)

  pickle.dump(dcomp,open(dpickle,'wb'),pickle.HIGHEST_PROTOCOL)

def main(**kwargs):
  dir=kwargs['base_directory']+kwargs['directory']
  id=kwargs['id']
  if kwargs['serial']: 
    file = glob.glob(dir+id+".????.vtk")
  else:
    file = glob.glob(dir+"id0/"+id+".????.vtk")
  file.sort()
  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
  else:
    start = 0
    end = len(file)
    fskip = 1


  file=file[start:end:fskip]

  if not os.path.isdir(dir+'png/'): os.mkdir(dir+'png/')
  if not os.path.isdir(dir+'pickles/'): os.mkdir(dir+'pickles/')

  ds = pa.AthenaDataSet(file[0])

  if start == 0:
    dspickle=dir+'pickles/'+id+'.0000.ds.p'
    f=open(dspickle,'wb')
    pickle.dump(ds,f,pickle.HIGHEST_PROTOCOL)
    f.close()

  fm = ds.domain['field_map']
  fields=ds.field_list + ds.derived_field_list
  for field in fm:
    if fm[field]['nvar'] == 3: 
      fields.remove(field)
      print field,' is removed from the list'

  grids=ds.grids

  for f in file:
    print 'Reading: ',f
    ds = pa.AthenaDataSet(f,grids=grids)
    fmtime = os.path.getmtime(f)

# slices and projection of fields

    if kwargs['slice_projection']:
      print 'Slicing and Projecting...'
      slicepickle='%s/pickles/%s.%s.%s.%1d.p' % (ds.dir,ds.id,ds.step,'surf',0)
      if os.path.isfile(slicepickle):
        pmtime=os.path.getmtime(slicepickle)
      else:
        pmtime=0.
      if pmtime < fmtime:

        for type in ('surf','slice'):
          for axis in range(3):
            slicepickle='%s/pickles/%s.%s.%s.%1d.p' % (ds.dir,ds.id,ds.step,type,axis)
            slice={}
            for field in fields:
              if type=='surf':
                slice[field] = pa.AthenaSurf(ds,axis=axis,field=field)
              else:
                slice[field] = pa.AthenaSlice(ds,axis=axis,field=field,center=[0.5,0.5,0.5])
            pickle.dump(slice,open(slicepickle,'wb'),pickle.HIGHEST_PROTOCOL)

# density decomposition
    if kwargs['density_decompose']:
      print 'Decomposing density...'
      dpickle='%s/pickles/%s.%s.%s.p' % (ds.dir,ds.id,ds.step,'den')
      if os.path.isfile(dpickle):
        pmtime=os.path.getmtime(dpickle)
      else:
        pmtime=0.
      if pmtime < fmtime: density_decomp(ds)

# turbulent B field
    if kwargs['turbulent_bfield']:
      if 'magnetic_field1' in fields:
        print 'Turbulent magnetic fields...'
        Bpickle='%s/pickles/%s.%s.%s.0.p' % (ds.dir,ds.id,ds.step,'Bturb')
        if os.path.isfile(Bpickle):
          pmtime=os.path.getmtime(Bpickle)
        else:
          pmtime=0.
        if pmtime < fmtime: turb_bfield(ds)

        print 'emf...'
        Bpickle='%s/pickles/%s.%s.%s.p' % (ds.dir,ds.id,ds.step,'emf')
        if os.path.isfile(Bpickle):
          pmtime=os.path.getmtime(Bpickle)
        else:
          pmtime=0.
        if pmtime < fmtime: dynamo(ds)


# helicity
    if kwargs['helicity']:
      print 'helicity calcuation...'
      hpickle='%s/pickles/%s.%s.%s.p' % (ds.dir,ds.id,ds.step,'helicity')
      if os.path.isfile(hpickle):
        pmtime=os.path.getmtime(hpickle)
      else:
        pmtime=0.
      if pmtime < fmtime: turb_helicity(ds)


if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-s','--serial',action='store_true',help='serial mode')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  parser.add_argument('-c','--density_decompose',action='store_true',help='density decomposition')
  parser.add_argument('-v','--helicity',action='store_true',help='helicity')
  parser.add_argument('-t','--turbulent_bfield',action='store_true',help='turbulent bfield')
  parser.add_argument('-p','--slice_projection',action='store_true',help='slice and projection')
  args = parser.parse_args()
  main(**vars(args))



