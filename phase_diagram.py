import pyathena
import numpy as np
from mpl_toolkits.axes_grid1 import ImageGrid
import matplotlib.pyplot as plt
import cooling

import astropy.constants as c
import astropy.units as u

def get_nP(file):
  ds = pyathena.AthenaDataSet(file)
  den = ds.read_all_data('density')
  Pok = ds.read_all_data('pressure')*ds.units['pressure'].cgs/c.k_B.cgs
  return den,Pok

def plot(itime):
  base='/scr1/cgkim/Research/athena_w_grackle/bin/SFR3D/id0/'
  files=[]
  files.append(base+'QA10_binf_4pc_grackle.%04d.vtk' % itime)
#  files.append(base+'SN_orig.%04d.vtk' % itime)
#  files.append(base+'SN_grackle.%04d.vtk' % itime)
#  files.append(base+'SN_grackle_cloudy.%04d.vtk' % itime)
#  files.append(base+'TI_orig.%04d.vtk' % itime)
#  files.append(base+'TI_grackle.%04d.vtk' % itime)

  for f in files:
    nden,pok = get_nP(f)
    plt.plot(nden.flatten(),pok.flatten(),'.')

  temp = np.logspace(1,5,200)
  cool = cooling.cool(temp)

  heat = 2.e-26
  nden = heat/cool
  plt.plot(nden,1.1*nden*temp)


  plt.setp(plt.gca(),'xscale','log')
  plt.setp(plt.gca(),'yscale','log')
  plt.setp(plt.gca(),'xlim',(1.e-2,1.e2))
  plt.setp(plt.gca(),'ylim',(1.e2,1.e8))

if __name__ == '__main__':
  plot(50)
