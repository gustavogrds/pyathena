#!/usr/bin/python

from init import *
import ath_hst
import glob
import os
from optparse import OptionParser

parser = OptionParser()
parser.add_option('-b','--base-directory', dest='base_directory',
                  help='base directory for data',
                  default='/scr1/cgkim/Research/athena_StarParticle/bin/')
parser.add_option('-d','--directory',dest='directory',
                  help='working directory',action='append')
parser.add_option('-i','--id',dest='id',
                  help='id of dataset',action='append')
parser.add_option('-o','--output',dest='out_fname',
                  help='output file name')
(options, args) = parser.parse_args()

files=[]
style = ['b-','r-','g-']

fig=plt.figure(figsize=(8,8))
ax1=fig.add_subplot(211)
ax2=fig.add_subplot(212)

t0=pc/1.e5/1.e6/yr # in units of Myr
mdot0=1.4*mp*pc**3/msun/t0 # in units of M_sun/Myr
vol = 64.*64.*32.

if options.directory == None: dirs=['']
else: dirs=options.directory
ids=options.id
for dir in dirs:
	for id in ids:
		fsearch = options.base_directory 
		fsearch = fsearch+dir
		fsearch = fsearch+id
		print fsearch
		hstfile = fsearch+'.hst'
		starfile = glob.glob(fsearch+'.*.star')

		hst = ath_hst.read(hstfile)
		keys = hst.keys()
		keys.sort()
		
		time = hst[keys[0]]
		mass = hst[keys[2]]
		if len(keys) == 15:
			msp = hst[keys[-3]]
			mghost1 = hst[keys[-2]]
		if len(keys) == 16:
			msp = hst[keys[-4]]
			mghost1 = hst[keys[-3]]
			mghost2 = hst[keys[-2]]

		print keys[0],keys[2],keys[-3],keys[-2]
		
		line,=ax1.plot(time,(mass+msp)/mass[0]-1)
		ax2.plot(time,mass,color=line.get_color())
		ax2.plot(time,msp,color=line.get_color(),ls='--')
		ax1.plot(time,mghost1,color=line.get_color())
		if len(keys) == 16: ax1.plot(time,mghost2,color=line.get_color(),ls=':')
		for file in starfile:
			star=ath_hst.read(file)
			keys=star.keys()
			keys.sort()
			time = star[keys[0]]
			mass = star[keys[1]]/vol
			
			ax2.plot(time,mass,color=line.get_color(),ls=':')

if options.out_fname == None: out_fname='png/mass_hst.png'
else: out_fname=options.out_fname


#ax1.set_xlim(30,70)
#ax2.set_xlim(30,70)
ax2.set_xlabel('t [Myr]')
ax1.set_ylabel(r'$M_{\rm tot}/M_0$')
ax2.set_ylabel(r'$M/V$')
ax2.set_yscale('log')
mystyle()
plt.tight_layout()
fig.savefig(out_fname,dpi=100)
