import numpy as np

def cool_lowT(temp):
	c1, c2, t1, t2=1.e7, 1.4e-2, 1.184e5, 92.

	lamtmp = c1*np.exp(-t1/(temp+1.e3)) \
             +c2*np.sqrt(temp)*np.exp(-t2/temp)
	return lamtmp*2.e-26
	
def cool(temp):
	c1, c2, t1, t2=1.e7, 1.4e-2, 1.184e5, 92.

	logt_crit=np.array([4.2, 4.35, 4.90, 5.40, 5.90, 6.25, 6.50, 7.50, np.inf])
	beta=np.array([-1.0, 1.63636, 0.0, -1.92, 0.0, -1.92, -0.34, 0.40])
	coeff=np.array([-17.55,-29.0182,-21.0,-10.632,-21.96,-9.96,-20.23,-25.78])

	temp=np.array(temp)
	lamtmp = np.zeros(temp.shape)
  	logt=np.log10(temp)

	ind = (logt < logt_crit[0])
	if ind.any():
		lamtmp[ind] = c1*np.exp(-t1/(temp[ind]+1.e3)) \
	       	      +c2*np.sqrt(temp[ind])*np.exp(-t2/temp[ind])
		lamtmp[ind] *= 2.e-26

	for i, b, c in zip(range(beta.size), beta, coeff):
		ind = (logt >= logt_crit[i]) & (logt < logt_crit[i+1])
		if ind.any():
			lamtmp[ind] = c+b*logt[ind]
			lamtmp[ind] = np.power(10.,lamtmp[ind])
	
	return lamtmp

def cool_nolog(temp):
	c1, c2, t1, t2=1.e7, 1.4e-2, 1.184e5, 92.
	t_crit=np.array([15849., 22387., 79433., 251189., 794328., 1778279., 3162278., 31622777., np.inf])
	beta=np.array([-1.0, 1.63636, 0.0, -1.92, 0.0, -1.92, -0.34, 0.40])
	coeff=np.array([2.8183829e-18, 9.5895891e-30, 1.e-21,2.33345806e-11,1.096478196e-22,1.096478196e-10,5.88843655e-21,1.65958691e-26])

	temp=np.array(temp)
	lamtmp = np.zeros(temp.shape)

	ind = (temp < t_crit[0])
	if ind.any():
		lamtmp[ind] = c1*np.exp(-t1/(temp[ind]+1.e3)) \
	       	      +c2*np.sqrt(temp[ind])*np.exp(-t2/temp[ind])
		lamtmp[ind] *= 2.e-26

	for i, b, c in zip(range(beta.size), beta, coeff):
		ind = (temp >= t_crit[i]) & (temp < t_crit[i+1])
		if ind.any():
			lamtmp[ind] = c*temp[ind]**b
	
	return lamtmp

def dcool(temp):
	c1, c2, t1, t2=1.e7, 1.4e-2, 1.184e5, 92.

	logt_crit=np.array([4.2, 4.35, 4.90, 5.40, 5.90, 6.25, 6.50, 7.50, np.inf])
	beta=np.array([-1.0, 1.63636, 0.0, -1.92, 0.0, -1.92, -0.34, 0.40])
	coeff=np.array([-17.55,-29.0182,-21.0,-10.632,-21.96,-9.96,-20.23,-25.78])

	temp=np.array(temp)
	lamtmp = np.zeros(temp.shape)
  	logt=np.log10(temp)

	ind = (logt < logt_crit[0])
	if ind.any():
		lamtmp[ind] = c1*t1*np.exp(-t1/(temp[ind]+1.e3))/(temp[ind]+1.e3)**2 \
	       	      +c2*(0.5/np.sqrt(temp[ind])+t2/temp[ind]**1.5)*np.exp(-t2/temp[ind])
		lamtmp[ind] *= 2.e-26

	for i, b, c in zip(range(beta.size), beta, coeff):
		ind = (logt >= logt_crit[i]) & (logt < logt_crit[i+1])
		if ind.any():
			lamtmp[ind] = c+b*logt[ind]
			lamtmp[ind] = b*np.power(10.,lamtmp[ind])/temp[ind]
	
	return lamtmp


