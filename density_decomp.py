from pyathena import *
import glob
import os
import argparse
import cPickle as pickle

# matplotlib
import matplotlib
matplotlib.use('agg')
from mpl_toolkits.axes_grid1 import AxesGrid,ImageGrid
import matplotlib.backends.backend_agg
import matplotlib.figure 
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import rc

# astropy
import astropy.constants as c
import astropy.units as u

def construct3d(array,shape,axis=0):
  if array.shape[0] != shape[axis]:
    print "Error: input array dimension (%d) should match with output dimension (%d) of axis %d" % (array.shape[0], shape[axis], axis)
    return -1
  else:
    if axis == 0:
      array_3d = np.tile(array.reshape(shape[0],1,1),(1,shape[1],shape[2]))
    elif axis == 1:
      array_3d = np.tile(array.reshape(1,shape[1],1),(shape[0],1,shape[2]))
    elif axis == 2:
      array_3d = np.tile(array.reshape(1,1,shape[2]),(shape[0],shape[1],shape[2]))
    return array_3d

def main(**kwargs):
  id=kwargs['id']
  if kwargs['directory'] == '': kwargs['directory'] = id+'/'
  dir=kwargs['base_directory']+kwargs['directory']
  if kwargs['serial']: 
    file = glob.glob(dir+id+".????.vtk")
  else:
    file = glob.glob(dir+"id0/"+id+".????.vtk")
  file.sort()
  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
  else:
    start = 0
    end = len(file)
    fskip = 1


  file=file[start:end:fskip]
  Tcold=184.
  Twarm=5050.
  Thot=2.e4

  if not os.path.isdir(dir+'png/'): os.mkdir(dir+'png/')
  if not os.path.isdir(dir+'pickles/'): os.mkdir(dir+'pickles/')

  dspickle=dir+'pickles/'+id+'.0000.ds.p'
  ds=pickle.load(open(dspickle,'rb'))
  grids=ds.grids

  for f in file:
    print 'Reading: ',f
    ds = AthenaDataSet(f,grids=grids)
    den = ds.read_all_data('density')
    temp = ds.read_all_data('temperature')

    dcomp={}
    cnm = temp <= Tcold
    unm = (temp > Tcold) * (temp <= Twarm)
    wnm = (temp > Twarm) * (temp <= Thot)
    him = temp > Thot
    dcnm3d = np.zeros(den.shape)
    dwnm3d = np.zeros(den.shape)
    dunm3d = np.zeros(den.shape)
    dhim3d = np.zeros(den.shape)
    dcnm3d[cnm]=den[cnm]
    dwnm3d[wnm]=den[wnm]
    dunm3d[unm]=den[unm]
    dhim3d[him]=den[him]
    
    dcomp['cnm']=dcnm3d.mean(axis=1).mean(axis=1)
    dcomp['wnm']=dwnm3d.mean(axis=1).mean(axis=1)
    dcomp['unm']=dunm3d.mean(axis=1).mean(axis=1)
    dcomp['him']=dhim3d.mean(axis=1).mean(axis=1)
    
    dpickle=dir+'pickles/'+'%s.%s.%s.p' % (ds.id,ds.step,'den')
    pickle.dump(dcomp,open(dpickle,'wb'),pickle.HIGHEST_PROTOCOL)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-s','--serial',action='store_true',help='serial mode')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  args = parser.parse_args()
  main(**vars(args))
