#!/usr/bin/python

from init import *
from ath_data import *

def cal_vr3d(grid,v):
	x1=grid['x1']
	x2=grid['x2']
	x3=grid['x3']
	nx1=grid['nx1']
	nx2=grid['nx2']
	nx3=grid['nx3']
	x3d = np.tile(x1.reshape(1,1,nx1),(nx3,nx2,1))
	y3d = np.tile(x2.reshape(1,nx2,1),(nx3,1,nx1))
	z3d = np.tile(x3.reshape(nx3,1,1),(1,nx2,nx1))

	r3d = np.sqrt(x3d**2+y3d**2+z3d**2)
	v1 = v[0]
	v2 = v[1]
	v3 = v[2]
	vr3d = (v1*x3d+v2*y3d+v3*z3d)/r3d

	return r3d.flatten(), vr3d.flatten()

def plot_all(axes,data,color='b'):
	x=data[0]
	for ax, y in zip(axes, data[1:]):
		ax.plot(x,y,marker='o',color=color)
	

dir='/scr1/cgkim/Research/athena_StarParticle/bin/'
data=ath_data(dir+'BEI/id0/BEI65T20.0000.bin')
data.readall()
cs=np.sqrt(1.1*kbol*20./1.4/mp)/1.e5

print cs
r3d, vr3d = cal_vr3d(data.grid,(data.data[1],data.data[2],data.data[3]))
ind=r3d.argsort()
d=data.data[0].flatten()
figdata=(r3d[ind],d[ind],vr3d[ind]/cs)
fig, axes=plt.subplots(nrows=1,ncols=2,figsize=(8,4))

axes = axes.flatten()
plot_all(axes,figdata)
for ax in axes[:-1]: ax.set_yscale('log')
for ax in axes: ax.set_xscale('log')
for ax in axes: ax.set_xlim(0,1)
axes[0].set_ylim(1.e2,1.e6)
axes[1].set_ylim(-10,0)

color='b'

for i in (5,12,20,21,22,23,24,30,22):
	data.set_step(i)
	data.readall()
	r3d, vr3d = cal_vr3d(data.grid,(data.data[1],data.data[2],data.data[3]))
	d=data.data[0].flatten()
	figdata=(r3d[ind],d[ind],vr3d[ind]/cs)
	plot_all(axes,figdata,color=color)
	if i == 22:
		plot_all(axes,figdata,color='r')
		r1d=np.logspace(-2,1,100)
		nHLP=8.86*1.1*kbol*20/(1.4*mp)**2/Gconst/r1d**2/pc**2/np.pi/4.0
		axes[0].plot(r1d,nHLP,color='r',ls=':')
		axes[1].axhline(-3.4,color='r',ls=':')
		color='g'
plt.tight_layout()
mystyle()
fig.savefig('rprof_iso.png',dpi=100)
