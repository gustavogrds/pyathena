from pyathena import *
import glob
import os
import argparse
import cPickle as pickle

# matplotlib
import matplotlib
matplotlib.use('agg')
from mpl_toolkits.axes_grid1 import AxesGrid,ImageGrid
import matplotlib.backends.backend_agg
import matplotlib.figure 
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import rc
from matplotlib import cm

# astropy
import astropy.constants as c
import astropy.units as u

# pandas
import pandas as pd

def texteffect():
  try:
    from matplotlib.patheffects import withStroke
    myeffect = withStroke(foreground="w", linewidth=3)
    kwargs = dict(path_effects=[myeffect], fontsize=12)
  except ImportError:
    kwargs = dict(fontsize=12)
  return kwargs

def draw_slice(slice,ax):
  im=ax.imshow(slice.data,origin='lower')
  im.set_extent(slice.bound)
  ax.set_xlabel(slice.axis_labels[0])
  ax.set_ylabel(slice.axis_labels[1])

  return im

def get_aux():
  aux={}
  aux['density']={'title':r'$n_{\rm H}$','cmap':'jet','cmin':5.e-3,'cmax':5.e2,'factor':1.0,'scale':'log'}
  aux['surface_density']={'title':r'$\Sigma$','cmap':'Spectral_r','cmin':1,'cmax':100,'factor':1.0,'scale':'log'}
  aux['number_density']={'title':r'$n_{\rm H}$','cmap':'RdYlBu_r','cmin':1.e-4,'cmax':5.e2,'factor':1.0,'scale':'log'}
  aux['pressure']={'title':r'$P_{\rm th}/k_B$','cmap':'hot','cmin':5.e0,'cmax':5.e4,'factor':1.0/c.k_B.cgs,'scale':'log'}
  aux['magnetic_pressure']={'title':r'$P_{\rm mag}/k_B$','cmap':'hot','cmin':5.e0,'cmax':5.e4,'factor':1.0/c.k_B.cgs,'scale':'log'}
  aux['turb_magnetic_pressure']={'title':r'$\delta P_{\rm mag}/k_B$','cmap':'hot','cmin':5.e0,'cmax':5.e4,'factor':1.0/c.k_B.cgs,'scale':'log'}
  aux['plasma_beta']={'title':r'$\beta$','cmap':'bone','cmin':5.e-2,'cmax':5.e2,'factor':1.0,'scale':'log'}
  aux['velocity1']={'title':r'$v_x$','cmap':'RdBu_r','cmin':-10,'cmax':10,'factor':1.e-5,'scale':'linear'}
  aux['velocity2']={'title':r'$v_y$','cmap':'RdBu_r','cmin':-10,'cmax':10,'factor':1.e-5,'scale':'linear'}
  aux['velocity3']={'title':r'$v_z$','cmap':'RdBu_r','cmin':-10,'cmax':10,'factor':1.e-5,'scale':'linear'}
  aux['magnetic_energy3']={'title':r'$P_{\rm mag,z}/k_B$','cmap':'Purples_r','cmin':5.e0,'cmax':5.e4,'factor':2.0/c.k_B.cgs,'scale':'log'}
  aux['kinetic_energy3']={'title':r'$P_{\rm turb,z}/k_B$','cmap':'hot','cmin':5.e0,'cmax':5.e4,'factor':2.0/c.k_B.cgs,'scale':'log'}
  aux['temperature']={'title':r'$T$','cmap':'Spectral_r','cmin':10,'cmax':1.e7,'factor':1.0,'scale':'log'}
  aux['sound_speed']={'title':r'$c_s$','cmap':'RdBu_r','cmin':1,'cmax':100,'factor':1.e-5,'scale':'log'}
  aux['alfven_velocity3']={'title':r'$v_{A,z}$','cmap':'RdBu_r','cmin':1,'cmax':100,'factor':1.e-5,'scale':'log'}

  return aux

def main(**kwargs):
  rc('text', usetex=True)
  dir=kwargs['base_directory']+kwargs['directory']
  id=kwargs['id']
  fields=kwargs['fields']
  file = glob.glob(dir+"id0/"+id+".????.vtk")
  file.sort()
  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
  else:
    start = 0
    end = len(file)
    fskip = 1

  new_mode=kwargs['new']


  aux = get_aux()
  units = {}

  if not os.path.isdir(dir+'png/'): os.mkdir(dir+'png/')
  if not os.path.isdir(dir+'pickles/'): os.mkdir(dir+'pickles/')

  dspickle=dir+'pickles/'+id+'.0000.ds.p'
  ds=pickle.load(open(dspickle,'rb'))
  grids=ds.grids

  xyratio=ds.domain['Lx'][2]/ds.domain['Lx'][1]
  ysize=12
  xsize=ysize/xyratio*len(fields)

  file=file[start:end:fskip]
  fig = matplotlib.figure.Figure(figsize=(xsize,ysize))

  Msun=(1.4*c.m_p/u.cm**3*u.pc**3).to('Msun').value
  Myr=(c.pc/u.km*u.s).to('Myr').value
#  cf=coolftn()

  for f in file:
    print 'Reading: ',f
    ds = AthenaDataSet(f,grids=grids)
    sp = pd.DataFrame(ds.read_starvtk())
    #for type in ['slice','surf']:
    for type in ['slice']:
      for axis in range(3):
        ImGrid = ImageGrid(fig, 111,\
                           nrows_ncols = (1, len(fields)),\
                           direction = "row",\
                           axes_pad = 0.1,\
                           label_mode = "L",\
                           share_all = False,\
                           cbar_location="top",\
                           cbar_mode="each",\
                           cbar_size="3%",\
                           cbar_pad="1%")
  
        
        slicepickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,type,axis)
        slice = pickle.load(open(slicepickle,'rb'))
        for iax,field in enumerate(fields):
          ax=ImGrid[iax]
          if field=='turb_magnetic_pressure':
            dslice=slice['magnetic_pressure']
            units['turb_magnetic_pressure'] = dslice.units.cgs
            if type == 'surf':
              Bpickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'Bturb',axis)
            elif type == 'slice':
              Bpickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'Bturbs',axis)
            Bturb = pickle.load(open(Bpickle,'rb'))
            dslice.data = Bturb[field]
          elif 'temperature' in field:
            dslice=slice['temperature1']
            dslice.data = cf.get_temp(dslice.data)
#          elif 'density' in field and axis == 0:
#            dslice=slice[field]
#            if type == 'slice':
#              surfpickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'surf',axis)
#              surf = pickle.load(open(surfpickle,'rb'))
#              dslice.data=surf['density'].data
#            else:
#              dslice.data=slice['density'].data
          else:
            dslice=slice[field]
          if not units.has_key(field): units[field]=dslice.units.cgs
          dslice.data *= units[field]
          dslice.data *= aux[field]['factor']
#          if 'density' in field and axis == 0: 
#            print 'surface_density',units[field]
#            dslice.data *= (1.4*c.m_p/u.cm**3*ds.domain['Lx'][2]*c.pc).to('Msun/pc^2')
          if len(sp) > 0: 
            
            if axis == 0:
              spx=sp['x1']
              spy=sp['x2']
              if type == 'slice': xbool=abs(sp['x3']) < ds.domain['dx'][2]*10
            elif axis == 1:
              spx=sp['x1']
              spy=sp['x3']
              if type == 'slice': xbool=abs(sp['x2']) < ds.domain['dx'][1]*10
            elif axis == 2:
              spx=sp['x2']
              spy=sp['x3']
              if type == 'slice': xbool=abs(sp['x1']) < ds.domain['dx'][0]*10
            spm=sp['mass']*Msun/100. 
            spa=sp['age']*Myr
            iyoung=np.where(spa < 40.)
            #ax.scatter(sp.x2,sp.x3,marker='o',c=np.log10(sp.mass*Msun),vmax=5,vmin=3,cmap=cm.jet)
            if type == 'slice':
              islab=np.where(xbool*(spa<40))
              ax.scatter(spx.loc[islab],spy.loc[islab],marker='o',\
                s=spm.loc[islab],c=spa.loc[islab],\
                vmax=40,vmin=0,cmap=cm.hot,alpha=1.0)
            ax.scatter(spx.loc[iyoung],spy.loc[iyoung],marker='o',\
              s=spm.loc[iyoung],c=spa.loc[iyoung],\
              vmax=40,vmin=0,cmap=cm.hot,alpha=0.5)
          im = draw_slice(dslice,ax)
          print field,dslice.data.min()
          faux=aux[field]
#          if 'density' in field and axis==0:
#            faux=aux['surface_density']
          if faux['cmin']>0: im.set_norm(LogNorm())
          im.set_cmap(plt.get_cmap(faux['cmap']))
          im.set_clim(faux['cmin'],faux['cmax'])
          cb=fig.colorbar(im,cax=ax.cax,orientation='horizontal')
          cb.set_label(faux['title'],**(texteffect()))
  
        ax=fig.axes[0]
        t1=ax.text(dslice.bound[0]*0.90,dslice.bound[3]*0.90,\
                   r'$t/t_{orb}$=%5.2f' % (ds.domain['time']/224.),\
                   ha="left", va="top", **(texteffect()))
        pngfname=dir+'png/%s.%s.%s.%1d.png' % (ds.id,ds.step,type,axis)
        canvas = matplotlib.backends.backend_agg.FigureCanvasAgg(fig)
        canvas.print_figure(pngfname,bbox_inches='tight')
        fig.clf()
        print 'Output: ',pngfname

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('fields',type=str,nargs='*',
                      default=['density','pressure','magnetic_pressure','plasma_beta'],
                      help='fields')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  parser.add_argument('--new',action='store_true',default=False,
                      help='toggle to store pickle files')
  args = parser.parse_args()
  main(**vars(args))



