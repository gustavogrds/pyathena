from init import *
from cooling import *
import BE_cool
import os
import glob
import ath_hst
from scipy.signal import argrelextrema

def plot_MP(dataset,ax,iso=False,Pext=3000.):
	heat=2.e-26
	Mtot=[]
	Pc=[]
	for data in dataset:
		Tc = data[2][0]
        	nc = heat/cool_lowT(Tc)
        	rc = BE.r0*np.sqrt(Tc/nc)
        	mc = 4*np.pi*(rc*pc)**3*nc*1.4*mp/msun

    		MPext=interp1d(data[3][::-1],data[4][::-1])
    		TPext=interp1d(data[3][::-1],data[2][::-1])

		Te = TPext(Pext)
       		ne = Pext/1.1/Te
       		re = BE.r0*np.sqrt(Te/ne)
       		me = 4*np.pi*(re*pc)**3*ne*1.4*mp/msun

       		if Pext>data[3].max(): Mtot.append(0.)
       		else: Mtot.append(MPext(Pext))
    		Pc.append(data[3][0]/Pext)

	Mtot=np.array(Mtot)
	Pc=np.array(Pc)

	lines=ax.plot(Pc,Mtot,marker='o')
	ax.set_xlabel('$P_c/P_{ext}$')
	ax.set_ylabel('$M_{tot}/M_{\odot}$')
	ax.set_xscale('log')
	mystyle()
	leg=ax.legend()


dir='/scr1/cgkim/Research/athena/bin/BE_cool/id0/'
hstfiles=glob.glob(dir+'*.hst')
hstfiles.sort()

hsts=[]
for file in hstfiles:
	basename=os.path.basename(file)
	if basename.startswith('BEU'):
		hsts.append(ath_hst.read(file))

fig=plt.gcf()
fig.clf()

Mtot_num=[]
Pc_num=[]
Punit=1.4*mp*1.e10/kbol
ax1=fig.add_subplot(211)

ax2=fig.add_subplot(212)

try:
	print len(data)
except NameError:	
	BE = BE_cool.BEsolution(3000.)
	data, dataiso = BE.getall()

plot_MP(data,ax2,Pext=3000.)

for hst in hsts:
	print len(hst['time']),len(hst['Pmax'])
	Mcore=np.array(hst['Mcore'])
	Pmax=np.array(hst['Pmax'])
	if Pmax.max() < 2.e5:
		Mcore=np.array([Mcore[0],Mcore[400:].mean()])*np.array(hst['vol'])
		Pmax=np.array([Pmax[0],Pmax[400:].mean()])/3000.
		ax2.plot(Pmax,Mcore,marker='o',color='r',ls=':')
		ax1.plot(hst['time'],hst['Pmax'],color='r')
	else:
		Mcore=np.array([Mcore[0],Mcore[-1]])*np.array(hst['vol'])
		Pmax=np.array([Pmax[0],Pmax[-1]])/3000.
		ax2.plot(Pmax,Mcore,marker='o',color='g',ls=':')
		ax1.plot(hst['time'],hst['Pmax'],color='g')


ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlim(1,100)
ax2.set_xscale('log')
ax2.set_ylim(0,300)
ax1.set_xlabel(r't')
ax1.set_ylabel(r'$P_c/k$')

plt.tight_layout()
