#!python
"""
%   AUTHOR:  Chang-Goo Kim 
%   LAST MODIFIED:  01/20/2014
%
%   PURPOSE: Read data dump from Athena
%   USAGE:
%
%   from ath_data import *
%
%   ath = ath_data(filename)
%
% set grid and read all variables.
% grid is stored as dictionary in ath.grid
% data and its labels are stored as lists in ath.data and ath.dlabel, respectively
%
%   ath.readall()
%   den = ath.data[0]
%   v1 = ath.data[1]
%   v2 = ath.data[2]
%   v3 = ath.data[3]
%   P = ath.data[4]
%
% each variables can be read separately using readvar() function.
%
%   ath.set_grid()
%   den = ath.readvar(ath.grid,'d')
%
% binary and vtk dumps are distinguished based on the file extention.
% grid dictionary from each file format shares important information for grid structure,
% but not exactly same.
%
% different time step can be reloaded simply by using set_step.
% if grid is already set, it will not be reset in readall() function.
%   
%   ath.set_step(10)
%   ath.readall()
%
"""

import ath_util
import struct
import numpy as np
import string

class ath_data(object):
	def __init__(self,filename):
		self.setup(filename)

	def setup(self,filename=None):
		if filename != None:
			dir, id, step, ext = ath_util.parse_filename(filename)
			self.dir=dir
			self.id=id
			self.step=step
			self.ext=ext
			self.set_filename()
		self.grid=None
		self.data = []
		self.dlabel = []

	def set_filename(self):
		step='.%04d.' % self.step
		import os
		self.filename=os.path.join(self.dir,self.id)+step+self.ext
		self.base=self.id+step

	def set_ext(self,ext):
		if self.ext != ext:
			self.ext=ext
			self.set_filename()

	def set_dir(self,dir):
		if self.dir != dir:
			self.dir=dir
			self.set_filename()

	def set_id(self,id):
		if self.id != id:
			self.id=id
			self.set_filename()

	def set_step(self,step):
		if self.step != step:
			self.step=step
			self.set_filename()

	def set_grid(self):
		if self.ext == 'bin':
			self.grid = self.init_grid_bin()
		elif self.ext == 'vtk':
			self.grid = self.init_grid_vtk()
		self.time = self.grid['time']

	def readvar(self,grid=None,dlabel='d'):
		if self.grid == None: 
			self.set_grid()
		if grid == None: 
			grid = self.grid

		if self.ext == 'bin':
			data=self.readbin(grid,dlabel,dtype='d')
		if self.ext == 'vtk':
			if dlabel == 'd': varname='density'
			if dlabel == 'M': varname='momentum'
			if dlabel == 'M1': varname='momentum'
			if dlabel == 'M2': varname='momentum'
			if dlabel == 'M3': varname='momentum'
			if dlabel == 'V': varname='velocity'
			if dlabel == 'V1': varname='velocity'
			if dlabel == 'V2': varname='velocity'
			if dlabel == 'V3': varname='velocity'
			if dlabel == 'P': varname='pressure'
			if dlabel == 'E': varname='total_energy'
			if dlabel == 'Phi': varname='gravitational_potential'
			data=self.readvtk(grid,varname,dtype='f')

			if dlabel == 'M1' and data != None: data = data[:,:,:,0]
			if dlabel == 'M2' and data != None: data = data[:,:,:,1]
			if dlabel == 'M3' and data != None: data = data[:,:,:,2]

			if dlabel == 'V1' and data != None: data = data[:,:,:,0]
			if dlabel == 'V2' and data != None: data = data[:,:,:,1]
			if dlabel == 'V3' and data != None: data = data[:,:,:,2]

		#if data == None: 
		#	print 'Error: cannot read varname '+dlabel

		return data

	def readall(self,grid=None):
		if self.grid == None: 
			self.set_grid()
		if grid == None: 
			grid = self.grid

		self.data = []
		self.dlabel = []

		time = self.readtime(grid)
		self.time = time

		d = self.readvar(grid,dlabel='d')
		self.data.append(d)
		self.dlabel.append('d')

		M1 = self.readvar(grid,'M1')
		if M1 != None: v1=M1/d
		else: v1 = self.readvar(grid,'V1')
		M2 = self.readvar(grid,'M2')
		if M2 != None: v2=M2/d
		else: v2 = self.readvar(grid,'V2')
		M3 = self.readvar(grid,'M3')
		if M3 != None: v3=M3/d
		else: v3 = self.readvar(grid,'V3')

		self.data.append(v1)
		self.dlabel.append('V1')
		self.data.append(v2)
		self.dlabel.append('V2')
		self.data.append(v3)
		self.dlabel.append('V3')

		E = self.readvar(grid,'E')
		if E != None:
			Ekin = 0.5*(v1*v1+v2*v2+v3*v3)*d
			if grid.has_key('gamma_1'): Gamma_1=grid['gamma_1']
			else: Gamma_1 = 5./3.
		        P = (E - Ekin)*Gamma_1
		else:
			P = self.readvar(grid,'P')

		if P != None:
			self.data.append(P)
			self.dlabel.append('P')

		Phi = self.readvar(grid,'Phi')
		if Phi != None:
			self.data.append(Phi)
			self.dlabel.append('Phi')
		#print 'Successfully read:'+string.join(self.dlabel)+' at t='+str(self.time)

	def init_grid_vtk(self,dtype='d'):
		"""
		#   AUTHOR:  Chang-Goo Kim
		#   LAST MODIFIED:  01/19/2014
		"""
        
		grid={}
        
		file = open(self.filename,'rb')
        
		# READ version and identifier
		# vtk DataFile Version 2.0
		tmp = file.readline()

		# READ header
		# CONSERVED vars at time= 0.000000e+00, level= 0, domain= 0
		tmp = file.readline()
		header = tmp.split()
		if (header[0] == 'CONSERVED'): vartype='cons'
		elif (header[0] == 'PRIMITIVE'): vartype='prim'
		grid['vartype'] = vartype

		time = eval(header[4])[0]
		print 'time = ',time
		grid['time']=time

		# READ file format
		# BINARY
		tmp = file.readline()

		# READ Dataset structure
		# DATASET STRUCTURED_POINTS
		tmp = file.readline()

		# READ dimension
		tmp = file.readline()
		dim = tmp.split()
		nx1 = eval(dim[1])-1
		nx2 = eval(dim[2])-1
		nx3 = eval(dim[3])-1
		if nx3>1: grid['ndim'] = 3
		elif nx2>1: grid['ndim'] = 2
		else: grid['ndim'] = 1

		#print 'ngrid = ',nx1,nx2,nx3 

		grid['nx1'] = nx1
		grid['nx2'] = nx2
		grid['nx3'] = nx3

		# READ origin
		tmp = file.readline()
		origins = tmp.split()
		x1min = eval(origins[1])
		x2min = eval(origins[2])
		x3min = eval(origins[3])
	
		grid['x1min']=x1min
		grid['x2min']=x2min
		grid['x3min']=x3min

		# READ spacing
		tmp = file.readline()
		spacing = tmp.split()
		dx1 = eval(spacing[1])
		dx2 = eval(spacing[2])
		dx3 = eval(spacing[3])
	
		grid['dx1']=dx1
		grid['dx2']=dx2
		grid['dx3']=dx3

		grid['x1max']=x1min+dx1*(nx1+1)
		grid['x2max']=x2min+dx2*(nx2+1)
		grid['x3max']=x3min+dx3*(nx3+1)

		# COMPUTE cell centered coordinates
		x0c = x1min+0.5*dx1
		y0c = x2min+0.5*dx2
		z0c = x3min+0.5*dx3
		x1 = np.arange(x0c,x0c+nx1*dx1,dx1)
		x2 = np.arange(y0c,y0c+nx2*dx2,dx2)
		x3 = np.arange(z0c,z0c+nx3*dx3,dx3)

		grid['x1']=x1
		grid['x2']=x2
		grid['x3']=x3

		# READ number of cell data
		# CELL_DATA 4096
		tmp = file.readline()

		# DATA starts here
		data_offset = file.tell()
		grid['data_offset']=data_offset
        
		# CLOSE FILE
		file.seek(0,2)
		eof = file.tell()
		grid['eof']=eof
		file.close()
        
		return grid

	def readvtk(self,grid,varname='density',dtype='f'):

		nx1 = grid['nx1']
		nx2 = grid['nx2']
		nx3 = grid['nx3']
		Ntot = nx1*nx2*nx3

		varname_avail=[]

		file=open(self.filename,'rb')

		offset = grid['data_offset']
		file.seek(0,2)
		eof = file.tell()
		file.seek(offset)

		while offset < eof:

			tmp = file.readline()
			header = tmp.split()

			if header[0] == 'SCALARS':
				tmp=file.readline() # read one more line "LOOK UP TABLE"
				nvar = 1
			elif header[0] == 'VECTORS':
				nvar = 3
			else:
				print 'Error: '+header[0] + ' is unknown type'

			dsize = nvar * Ntot * struct.calcsize(dtype)	

			if header[1] == varname:
				#print 'Reading: '+string.join(header)
				data=file.read(dsize)
				# read in BIG ENDIAN ORDER
				var = np.asarray(struct.unpack('>'+nvar*Ntot*dtype,data)) 
				if nvar == 1: var.shape = (nx3, nx2, nx1)
				else: var.shape = (nx3, nx2, nx1, nvar)
				file.close()
				return var
			else:
				varname_avail.append(header[1])
				file.seek(dsize,1)
				file.readline()
				offset = file.tell()
 
		#print 'Error: '+varname+' is unknow varname!'
		#print 'Available Varnames:'+string.join(varname_avail,',')
		file.close()


	def init_grid_bin(self,dtype='d'):
		"""
		#   AUTHOR:  Chang-Goo Kim after ath_init_grid.m by Aaron Skinner
		#   LAST MODIFIED:  11/19/2013
		"""
        
		grid={}
        
		file = open(self.filename,'rb')
        
		# READ coord, nx1, nx2, nx3, nvar, nscalars, ngrav, npart
		ndat=8
		offset=0
		s_size=struct.calcsize(ndat*'i')
		data = file.read(s_size) 
        
		coord, nx1, nx2, nx3, nvar, nscalars, ngrav, npart \
		 = struct.unpack_from(ndat*'i',data,offset)
		#print 'ngrid = ',nx1,nx2,nx3 
        
		grid['nx1'] = nx1
		grid['nx2'] = nx2
		grid['nx3'] = nx3
		grid['nvar'] = nvar
		grid['nscalars'] = (nscalars != 0)
		grid['ngrav'] = (ngrav == 1)
		grid['npart'] = (npart == 1)
        
		# READ (Gamma-1), sound speed
		ndat=2
		s_size=struct.calcsize(ndat*dtype)
		data = file.read(s_size) 
		
		gamma_1, iso_csound \
		 = struct.unpack(ndat*dtype,data)

		time_offset = file.tell()
 		# READ time, dt
		ndat=2
		s_size=struct.calcsize(ndat*dtype)
		data = file.read(s_size) 

		time, dt \
		 = struct.unpack(ndat*dtype,data)

		#print 'time, dt = ', time, dt
			
		grid['gamma_1']=gamma_1
		grid['iso_csound']=iso_csound
		grid['time']=time
		grid['dt']=dt

		grid['time_offset'] = time_offset
        
		# READ x1, x2, x3 coordinates
		s_size=struct.calcsize(nx1*dtype)
		data = file.read(s_size) 
		x1 = np.asarray(struct.unpack(nx1*dtype,data))
        
		s_size=struct.calcsize(nx2*dtype)
		data = file.read(s_size) 
		x2 = np.asarray(struct.unpack(nx2*dtype,data))
        
		s_size=struct.calcsize(nx3*dtype)
		data = file.read(s_size) 
		x3 = np.asarray(struct.unpack(nx3*dtype,data))
        
		data_offset = file.tell()
        
		grid['x1']=x1
		grid['x2']=x2
		grid['x3']=x3
        
		# CLOSE FILE
		file.close()
        
		# COMPUTE dx1, dx2, dx3
		dx1 = 0.0; dx2 = 0.0; dx3 = 0.0
		ndim=0
		if (nx1>1):
			dx1 = (x1.max()-x1.min())/(nx1-1)
			ndim+=1
		if (nx2>1):
			dx2 = (x2.max()-x2.min())/(nx2-1)
			ndim+=1
		if (nx3>1):
			dx3 = (x3.max()-x3.min())/(nx3-1)
			ndim+=1
        
		grid['dx1']=dx1
		grid['dx2']=dx2
		grid['dx3']=dx3
		grid['ndim']=ndim
        
		# COMPUTE min and max of x1, x2, x3
		x1min, x1max = x1.min() - 0.5*dx1, x1.max() + 0.5*dx1
		x2min, x2max = x2.min() - 0.5*dx2, x2.max() + 0.5*dx2
		x3min, x3max = x3.min() - 0.5*dx3, x3.max() + 0.5*dx3
        
		grid['x1min']=x1min
		grid['x1max']=x1max
		grid['x2min']=x2min
		grid['x2max']=x2max
		grid['x3min']=x3min
		grid['x3max']=x3max
        
		grid['data_offset']=data_offset
        
		grid['nad'] = (nvar==5 or nvar == 8)
		grid['nmhd'] = (nvar==7 or nvar == 8)
        
		return grid

	def readtime(self,grid):

		file=open(self.filename,'rb')
		if self.ext == 'bin':
			dtype='d'
			ndat=2
			s_size=struct.calcsize(ndat*dtype)
			toffset = grid['time_offset']
			file.seek(toffset)
			data = file.read(s_size) 
			time, dt = struct.unpack(ndat*dtype,data)

		if self.ext == 'vtk':
			tmp = file.readline()
			tmp = file.readline()
			header = tmp.split()
			time = eval(header[4])[0]

		file.close()

		return time
 
	def readbin(self,grid,varname,dtype='d',time=False):
		"""
		%   AUTHOR:  Chang-Goo Kim after ath_read.m by Aaron Skinner
		%   LAST MODIFIED:  11/18/2013
		"""
        
		nx1 = grid['nx1']
		nx2 = grid['nx2']
		nx3 = grid['nx3']
		Ntot = nx1*nx2*nx3
        
		eskip = grid['nad']
		bskip = 3*(grid['nmhd'])
		gskip = grid['ngrav']
        
		adiabatic = eskip > 0
		mhd = bskip > 0
		selfg = gskip > 0
        
		if varname == 'd':
			skip = 0 # d is first
		elif varname == 'M1' or varname == 'V1':
			skip = 1 # skip for d
		elif varname == 'M2' or varname == 'V2':
			skip = 2 # skip for d, M1
		elif varname == 'M3' or varname == 'V3':
			skip = 3 # skip for d, M1, M2
		elif adiabatic and (varname == 'E' or varname == 'P'):
			skip = 4 # skip for d, M1, M2, M3	
		elif mhd and (varname == 'B1'):
			skip = eskip + 4
		elif mhd and (varname == 'B2'):
			skip = eskip + 5
		elif mhd and (varname == 'B3'):
			skip = eskip + 6
		elif selfg and (varname == 'Phi'):
			skip = eskip + bskip + 4
		# ADD FOR RADIATION LATER
		else:
			status = -1
			#print '%s is not a valid variable name!\n' % varname
			return
        
		file=open(self.filename,'rb')
		dsize = Ntot * struct.calcsize(dtype)	
		offset = grid['data_offset'] + skip * dsize
		file.seek(offset)
		data=file.read(dsize)
        
		var = np.asarray(struct.unpack(Ntot*dtype,data))
		var.shape = (nx3, nx2, nx1)

		file.close()
        
		return var

	def rprof(self,x0=0.,y0=0.,z0=0.):
		if self.grid == None: 
			self.set_grid()

		x1=self.grid['x1']
		x2=self.grid['x2']
		x3=self.grid['x3']

		nx1=self.grid['nx1']
		nx2=self.grid['nx2']
		nx3=self.grid['nx3']

		x3d = np.tile(x1.reshape(1,1,nx1),(nx3,nx2,1))-x0
		y3d = np.tile(x2.reshape(1,nx2,1),(nx3,1,nx1))-y0
		z3d = np.tile(x3.reshape(nx3,1,1),(1,nx2,nx1))-z0

		r3d = np.sqrt((x3d)**2+(y3d)**2+(z3d)**2)

		return r3d,x3d,y3d,z3d

def readvtk_star(filename,dtype='f'):

	file=open(filename,'rb')

	file.seek(0,2)
	eof = file.tell()
	file.seek(0,0)

	# READ version and identifier
	# vtk DataFile Version 2.0
	tmp = file.readline()

	# READ header
	# STAR PARTICLES at time= %e\n
	# CONSERVED vars at time= 0.000000e+00, level= 0, domain= 0
	tmp = file.readline()
	header = tmp.split()
	if (header[0] == 'STAR'): vartype='star'

	time = eval(header[4])
	#print 'time = ',time

	# READ file format
	# BINARY
	tmp = file.readline()

	# READ Dataset structure
	# DATASET UNSTRUCTURED_GRID
	tmp = file.readline()

	# READ position
	# POINTS Nstar float
	tmp = file.readline()
	tmp = tmp.split()
	nstar = eval(tmp[1])

	#print 'nstar = ',nstar

	dsize = nstar * 3 * struct.calcsize(dtype)	

	data=file.read(dsize)
	# read in BIG ENDIAN ORDER
	pos = np.asarray(struct.unpack('>'+nstar*3*dtype,data)) 

	# READ VTK_VERTEX
	# CELLS nstar 2*nstar
	tmp = file.readline()
	dsize = nstar * 2 * struct.calcsize('i')	
	data=file.read(dsize)

	# READ blank
	tmp = file.readline()

	# CELL_TYPE nstar
	tmp = file.readline()
	dsize = nstar * 1 * struct.calcsize('i')	
	data=file.read(dsize)

	# READ blank
	tmp = file.readline()

	# READ time and step
	# FIELD FieldData 2
	tmp = file.readline()

	# TIME 1 1 float
	tmp = file.readline()
	dsize = 1 * struct.calcsize('f')
	data=file.read(dsize)
	# READ blank
	tmp = file.readline()

	# CYCLE 1 1 int
	tmp = file.readline()
	dsize = 1 * struct.calcsize('i')
	data=file.read(dsize)
	# READ blank
	tmp = file.readline()

	# CELL_DATA nstars
	tmp = file.readline()
	# POINT_DATA nstars
	tmp = file.readline()

	# READ id
	# SCALARS star_particle_id int
	tmp = file.readline()
	# LOOKUP_TABLE default
	tmp = file.readline()

	dsize = nstar * 1 * struct.calcsize('i')
	data=file.read(dsize)
	# read in BIG ENDIAN ORDER
	id = np.asarray(struct.unpack('>'+nstar*1*'i',data)) 
	# READ blank
	tmp = file.readline()

	# READ mass
	# SCALARS star_particle_mass float
	tmp = file.readline()
	# LOOKUP_TABLE default
	tmp = file.readline()

	dsize = nstar * 1 * struct.calcsize('f')
	data=file.read(dsize)
	# read in BIG ENDIAN ORDER
	mass = np.asarray(struct.unpack('>'+nstar*1*'f',data)) 
	# READ blank
	tmp = file.readline()

	# READ vel
	# VECTORS star_particle_velocity float
	tmp = file.readline()

	dsize = nstar * 3 * struct.calcsize('f')
	data=file.read(dsize)
	# read in BIG ENDIAN ORDER
	vel = np.asarray(struct.unpack('>'+nstar*3*'f',data)) 

	# READ blank
	tmp = file.readline()

 	if file.tell() != eof: print 'too few bytes read'

	file.close()

	return nstar, id, mass, pos, vel
