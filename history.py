import ath_hst
import glob
import os
import argparse

import cPickle as pickle

# matplotlib
import matplotlib
matplotlib.use('agg')
from mpl_toolkits.axes_grid1 import AxesGrid,ImageGrid
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

# numpy
import numpy as np

# astropy
import astropy.constants as c
import astropy.units as u

# utility
import myutilities as myutil

def texteffect():
  try:
    from matplotlib.patheffects import withStroke
    myeffect = withStroke(foreground="w", linewidth=3)
    kwargs = dict(path_effects=[myeffect], fontsize=12)
  except ImportError:
    kwargs = dict(fontsize=12)
  return kwargs

def hst_with_unit(hst,volume=512.*512.*1024):
  dunit=1.4*c.m_p/u.cm**3
  lunit=u.pc
  vunit=u.km/u.s
  tunit=lunit/vunit
  punit=(dunit*vunit**2/c.k_B).cgs

  hstwu={}
  for k in hst: hstwu[k]=hst[k]
  hstwu['time']=hst['time']*tunit.to('Myr')
  hstwu['dt']=hst['dt']*tunit.to('Myr')
  vol=volume*lunit**3
  hstwu['mass']=hst['mass']*(dunit*vol).to('Msun')
  hstwu['dmass']=hst['mass']/hst['mass'][0]-1

  if 'vx2' in hst:
    hstwu['vx']=np.sqrt(hst['vx2']/hst['mass'])*vunit
    hstwu['vy']=np.sqrt(hst['vy2']/hst['mass'])*vunit
    hstwu['vz']=np.sqrt(hst['vz2']/hst['mass'])*vunit
  if 'H2' in hst:
    hstwu['H']=np.sqrt(hst['H2']/hst['mass'])*lunit
  if 'Mgbc' in hst:
    mdiff=hst['mass']-hst['Mgbc']
    hstwu['Hdiff']=np.sqrt(hst['Hdiff2']/mdiff)*lunit
    hstwu['vxdiff']=np.sqrt(hst['vx_diff2']/mdiff)*vunit
    hstwu['vydiff']=np.sqrt(hst['vy_diff2']/mdiff)*vunit
    hstwu['vzdiff']=np.sqrt(hst['vz_diff2']/mdiff)*vunit
    hstwu['vthdiff']=np.sqrt(hst['vth_diff2']/mdiff)*vunit
    hstwu['fdiff']=mdiff/hst['mass']
    hstwu['fgbc']=hst['Mgbc']/hst['mass']
    hstwu['fw']=hst['Mw']/mdiff
    hstwu['fu']=hst['Mu']/mdiff
    hstwu['fc']=hst['Mc']/mdiff
  if 'Pmid_diff' in hst:
    hstwu['Pmid_diff']=hst['Pmid_diff']/hst['volmid_diff']*punit
    hstwu['Pturb_diff']=hst['Pturb_diff']/hst['volmid_diff']*punit
    hstwu['Ptot_diff']=hstwu['Pmid_diff']+hstwu['Pturb_diff']
    hstwu['nmid']=hst['rhomid_diff']/hst['volmid_diff']/u.cm**3
  if 'Pmag_diff' in hst:
    hstwu['Pmag_diff']=hst['Pmag_diff']/hst['volmid_diff']*punit
    hstwu['Ptot_diff'] += hstwu['Pmag_diff']
  if 'x1ME' in hst:
    hstwu['x1ME']=hst['x1ME']*(vol*dunit*vunit**2).cgs
    hstwu['x2ME']=hst['x2ME']*(vol*dunit*vunit**2).cgs
    hstwu['x3ME']=hst['x3ME']*(vol*dunit*vunit**2).cgs
    hstwu['ME']=hstwu['x1ME']+hstwu['x2ME']+hstwu['x3ME']
    hstwu['vA1']=np.sqrt(2.0*hst['x1ME']/hst['mass'])*vunit
    hstwu['vA2']=np.sqrt(2.0*hst['x2ME']/hst['mass'])*vunit
    hstwu['vA3']=np.sqrt(2.0*hst['x3ME']/hst['mass'])*vunit
  if 'sfr' in hst:
    hstwu['sfr']=(hst['sfr']*u.M_sun/u.pc**2/u.Myr)
  if 'x1KE' in hst:
    hstwu['x1KE']=hst['x1KE']*(vol*dunit*vunit**2).cgs
    hstwu['x2KE']=hst['x2KE']*(vol*dunit*vunit**2).cgs
    hstwu['x3KE']=hst['x3KE']*(vol*dunit*vunit**2).cgs
    hstwu['KE']=hstwu['x1KE']+hstwu['x2KE']+hstwu['x3KE']
    hstwu['totalE']=hst['totalE']*(vol*dunit*vunit**2).cgs
    hstwu['TE']=hstwu['totalE']-hstwu['KE']
    hstwu['RKE']=hstwu['KE']/hstwu['totalE']/0.283
    hstwu['RTE']=hstwu['TE']/hstwu['totalE']/0.717
  if 'r_mom' in hst:
    hstwu['r_mom']=hst['r_mom']*(vol*(dunit*vunit)).to('Msun*km/s')
  if 'Mhot' in hst:
    hstwu['Mhot']=hst['Mhot']*(vol*dunit).to('Msun')
    hstwu['Msh']=hst['Msh']*(vol*dunit).to('Msun')
  if 'Rsh' in hst:
    hstwu['Rsh']=hst['Rsh']/hst['Msh']

  import pandas as pd 
  return pd.DataFrame(hstwu)

def sn_with_unit(sn,area=512.*512.,time=None):
  lunit=u.pc
  vunit=u.km/u.s
  tunit=lunit/vunit

  tbin = 10.
  msn = 100.
  sntime = sn['time']*tunit.to('Myr')
  if time == None: time=np.arange(0,max(sntime),0.1)
  sfr=np.arange(len(time),dtype='float32')

  for i in range(len(time)):
    snind=np.where((sntime > time[i]-tbin) & (sntime <=time[i]))
    numSN=sn['Nstarmerg'][snind[0]].sum()
    sfr[i] = numSN*msn/tbin/area
  snwu={}
  snwu['sfr_time']=time
  snwu['sfr']=sfr

  import pandas as pd 
  return pd.DataFrame(snwu)
    
class hstdata(object):
  def __init__(self,file=None):

    if file is None :
      base='/scr1/cgkim/SFR_MHD/hst/'

      hstfiles=[]
      hstfiles.append(base+'QA10.hst')
      hstfiles.append(base+'QA10_binf_Turb_SN_2pc.hst')
      hstfiles.append(base+'QA10_b10_Turb_SN_2pc_p4r2.hst')
      hstfiles.append(base+'QA10_b10_Turb_SN_2pc_p3.hst')
      hstfiles.append(base+'QA10_b1_Turb_SN_2pc_p4r2.hst')
      hstfiles.append(base+'QA10_b1_Turb_SN_2pc_p3r2.hst')

      labels=[]
      labels.append('QA10')
      labels.append('QA10_HO')
      labels.append('QA10_MA10')
      labels.append('QA10_MB10')
      labels.append('QA10_MA1')
      labels.append('QA10_MB1')

    else:
      hstfiles=file
      labels=[]
      for f in file: labels.append(os.path.basename(f))
    self.hstfiles=hstfiles
    self.labels=labels
  
  def read(self,area=512.*512.):

    hst={}
    hstwu={}
    sn={}
    snwu={}

    import pandas as pd

    for f,key in zip(self.hstfiles,self.labels):
      if os.path.isfile(f):
        hstmtime=os.path.getmtime(f)
        hst[key]=ath_hst.read_w_pandas(f)
        volume=ath_hst.get_volume(f)
        hstwu[key]=hst_with_unit(hst[key],volume=volume)

	snfile=str.replace(f,'.hst','.sn')
	if os.path.exists(snfile):
          sn[key]=ath_hst.read_w_pandas(snfile,snfile=True)
  	  if os.path.isfile(snfile+'wu.p'):
            pmtime=os.path.getmtime(snfile+'wu.p')
          else:
            pmtime=0.
          if pmtime < hstmtime:
            snpd=sn_with_unit(sn[key],area=area)
	    snpd.to_pickle(snfile+'wu.p')
          snwu[key]=pd.read_pickle(snfile+'wu.p')

    self.hst=hst
    self.hstwu=hstwu
    self.sn=sn
    self.snwu=snwu

def plot(data,ykeys=['sfr'],factor={},fig=None,smooth=False):
    if fig == None: fig=plt.figure(1,figsize=(12,15))
    plt.clf()
    axes={}
    nkeys=len(ykeys)
    nrow=nkeys
    ncol=1
    if nkeys > 5 and nkeys<=8:
      ncol=2
      nrow=nkeys/ncol
    elif nkeys>8:
      ncol=3
      nrow=nkeys/ncol

    for iax,ykey in enumerate(ykeys):
      ax=plt.subplot(nrow,ncol,iax+1)
      axes[ykey]=ax
      if factor.has_key(ykey): f=factor[ykey]
      else: f=1.0
      for k in data.labels:
        if k in data.hstwu: 
          h=data.hstwu[k]
          if ykey in h: 
            if smooth:
              ax.plot(myutil.smooth(h['time'][::20]),myutil.smooth(h[ykey][::20]*f),label=k)
            else:
              ax.plot(h['time'],h[ykey]*f,label=k)
          else: ax._get_lines.color_cycle.next()
      ax.set_xlabel('time')
      ax.set_ylabel(ykey)

    axes[ykeys[0]].legend(loc=0)
    return fig,axes

def main(**kwargs):
  hst=hstdata()

  hst.hstfiles=[]
  base='/tigress/changgoo/'
#  hst.hstfiles.append(base+'QA10_binf_Turb_SN_2pc/id0/QA10_binf_Turb_SN_2pc.hst')
#  hst.hstfiles.append(base+'QA10_b10_Turb_SN_2pc_p4/id0/QA10_b10_Turb_SN_2pc_p4r2.hst')
#  hst.hstfiles.append(base+'QA10_b10_Turb_SN_2pc_p3/id0/QA10_b10_Turb_SN_2pc_p3r2.hst')
#  hst.hstfiles.append(base+'QA10_b1_Turb_SN_2pc_p4/id0/QA10_b1_Turb_SN_2pc_p4r2.hst')
#  hst.hstfiles.append(base+'QA10_b1_Turb_SN_2pc_p3/id0/QA10_b1_Turb_SN_2pc_p3r2.hst')
  hst.hstfiles.append(base+'QA10_b100_Turb_SN_2pc_p3/id0/QA10_b100_Turb_SN_2pc_p3.hst')
  hst.hstfiles.append(base+'QA10_b100_Turb_SN_2pc_p3/id0/QA10_b6_Turb_SN_2pc_p3.hst')
  hst.hstfiles.append(base+'QA10_b100_Turb_SN_2pc_p3_nr/id0/QA10_b100_Turb_SN_2pc_p3_nr.hst')

  hst.labels=[]
#  hst.labels.append('QA10_HO')
#  hst.labels.append('QA10_MA10')
#  hst.labels.append('QA10_MB10')
#  hst.labels.append('QA10_MA1')
#  hst.labels.append('QA10_MB1')
#  hst.labels.append('QA10_MB100')
  hst.labels.append('MB100')
  hst.labels.append('MB6')
  hst.labels.append('MB100NR')

  print hst.hstfiles
  hst.read()
  fig,axes=plot(hst,ykeys=['mass','sfr','H','x1ME','x2ME','x3ME','x1KE','x2KE','x3KE',],factor={'mass':1/(512.*512.)})
  for ax in fig.axes: ax.grid(True)
  
  plt.tight_layout()
  plt.setp(fig.axes,'xlim',(0,1000))
  axes['sfr'].set_yscale('log')
#  axes['x2ME'].set_yscale('log')
#  axes['dt'].set_yscale('log')
  ax=axes['sfr']
  lines=ax.lines
  sn=hst.snwu
  
#  plt.savefig('/tigress/changgoo/public_html/history.png',bbox_inches='tight')
  

if __name__ == '__main__':
  main()
