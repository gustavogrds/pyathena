import numpy as np
import argparse

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def cc_idx(domain,pos,periodic=True):
  le=domain['left_edge']
  dx=domain['dx']
  Nx=domain['Nx']
  if np.array(pos).ndim == 2:
    le=le[:,np.newaxis]
    dx=dx[:,np.newaxis]
    Nx=Nx[:,np.newaxis]
  idx=(pos-le-0.5*dx)/dx
  if periodic: 
    for i in range(3):
      idx[i]=np.fmod(idx[i],Nx[i])
      idx[idx < 0] += Nx[i] 
  return idx

def set_domain():
  domain={}
  domain['left_edge']=np.array([-256,-256,-512])
  domain['right_edge']=np.array([256,256,512])
  domain['dx']=np.array([2,2,2])
  domain['Lx']=domain['right_edge']-domain['left_edge']
  domain['Nx']=domain['Lx']/domain['dx']
  return domain

def interp3D(input_array,indices):
  output = np.empty(indices[0].shape)
  x_indices = indices[2]
  y_indices = indices[1]
  z_indices = indices[0]

  x0 = x_indices.astype(np.integer)
  y0 = y_indices.astype(np.integer)
  z0 = z_indices.astype(np.integer)
  x1 = x0 + 1
  y1 = y0 + 1
  z1 = z0 + 1

#Check if xyz1 is beyond array boundary:
  x1[np.where(x1==input_array.shape[0])] = 0
  y1[np.where(y1==input_array.shape[1])] = 0
  z1[np.where(z1==input_array.shape[2])] = 0

  x = x_indices - x0
  y = y_indices - y0
  z = z_indices - z0
  output = (input_array[x0,y0,z0]*(1-x)*(1-y)*(1-z) +
             input_array[x1,y0,z0]*x*(1-y)*(1-z) +
             input_array[x0,y1,z0]*(1-x)*y*(1-z) +
             input_array[x0,y0,z1]*(1-x)*(1-y)*z +
             input_array[x1,y0,z1]*x*(1-y)*z +
             input_array[x0,y1,z1]*(1-x)*y*z +
             input_array[x1,y1,z0]*x*y*(1-z) +
             input_array[x1,y1,z1]*x*y*z)
  return output

def los_idx(hat,domain,smin=0.,smax=3000.,ds=1.,vectorize=True,zmax_cut=True):
  zmax=domain['right_edge'][2]-0.5*domain['dx'][2]
  zmin=domain['left_edge'][2]+0.5*domain['dx'][2]
  xhat=hat[0]
  yhat=hat[1]
  zhat=hat[2]
# vectorlized version
  if vectorize:
    sarr=np.arange(smin,smax,ds)
    if zmax_cut:
      zarr=zhat*sarr
      ind = np.where((zarr < zmax)*(zarr > zmin))
      sarr = sarr[ind]
    xarr=xhat*sarr
    yarr=yhat*sarr
    zarr=zhat*sarr
    iarr = cc_idx(domain,[xarr,yarr,zarr])

  else:
# while loop for s from 0 to smax
    idx=[]
    s=ds
    sarr=[]
    while s < smax:
# get x,y,z by multiplying the path length s 
      x=xhat*s
      y=yhat*s
      z=zhat*s
      if zmax_cut and abs(z)>zmax: break
# get i,j,k by applying periodic BCs in x and y
      idx.append(cc_idx(domain,[x,y,z]))
      sarr.append(s)
      s = s+ds
# store indices and path lengthes
    iarr=np.array(idx).T
    sarr=np.array(sarr)

  return iarr,sarr

def main(**kwargs):
  import glob
  import os
  import pyathena as pa
  import pandas as pd
  import astropy.constants as c
  import astropy.units as u

  dir=kwargs['base_directory']+kwargs['directory']
  id=kwargs['id']
  if kwargs['serial']: 
    file = glob.glob(dir+id+".????.vtk")
  else:
    file = glob.glob(dir+"id0/"+id+".????.vtk")
  file.sort()
  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
  else:
    start = 0
    end = len(file)
    fskip = 1
  wlos=False
  if kwargs['write_los']:
    wlos=True


  file=file[start:end:fskip]

  if not os.path.isdir(dir+'png/'): os.mkdir(dir+'png/')
  if not os.path.isdir(dir+'pickles/'): os.mkdir(dir+'pickles/')
  if not os.path.isdir(dir+'los/'): os.mkdir(dir+'los/')

  ds = pa.AthenaDataSet(file[0])
  if start == 0:
    dspickle=dir+'pickles/'+id+'.0000.ds.p'
    f=open(dspickle,'wb')
    pickle.dump(ds,f,pickle.HIGHEST_PROTOCOL)
    f.close()

  fm = ds.domain['field_map']
  domain=ds.domain
  fields=ds.field_list
  mhd=False
  for field in fm:
    if fm[field]['nvar'] == 3: 
      fields.append(field+'1')
      fields.append(field+'2')
      fields.append(field+'3')
      fields.remove(field)
      print field,' is removed from the list'
      if field == 'magnetic_field': mhd=True
  fields.append('temperature')

  print len(fields)
  grids=ds.grids

# construct index array
  import healpy as hp
  nside=kwargs['nside']
  ipix = np.arange(hp.nside2npix(nside))

  smax=3000.
  deltas=domain['dx'][0]/2.
  NHfact=deltas*c.pc.cgs.value
  
# calculate stokes' parameter coefficient
  from astropy.analytic_functions import blackbody_lambda, blackbody_nu
  Bnu=blackbody_nu(353*u.GHz,18.*u.K).value
  dtau=deltas*c.pc.cgs.value*1.2e-26
  p0=0.2
  
  
  for f in file:
    print 'Reading: ',f
    ds = pa.AthenaDataSet(f,grids=grids)
    if wlos:
      if not os.path.isdir(dir+'los/%s.%d' % (ds.step,nside)): os.mkdir(dir+'los/%s.%d' % (ds.step,nside))
    data={}
    if mhd: map={'column_density':[],'temperature':[],'Blos':[],'I':[],'Q':[],'U':[],'v_r':[],'v_th':[],'v_phi':[]}
    else: map={'column_density':[],'temperature':[],'v_r':[],'v_th':[],'v_phi':[]}
    for fi in fields:
      print 'Field: ',fi
      data[fi]=ds.read_all_data(fi)
    for i in ipix:
      out = {}
      theta,phi = hp.pix2ang(nside,i)
      rhat=[np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)]
      thhat=[np.cos(theta)*np.cos(phi),np.cos(theta)*np.sin(phi),-np.sin(theta)]
      phhat=[-np.sin(phi),np.cos(phi),0]
      idx,sarr = los_idx(rhat,domain,smax=smax,ds=deltas)
      for fi in fields:
        out[fi]=interp3D(data[fi],idx)
      for hat,axis in zip([rhat,thhat,phhat],['Z','X','Y']):
        out['velocity_'+axis]=hat[0]*out['velocity1']+hat[1]*out['velocity2']+hat[2]*out['velocity3']
        if mhd: out['magnetic_field_'+axis]=hat[0]*out['magnetic_field1']+hat[1]*out['magnetic_field2']+hat[2]*out['magnetic_field3']
      if wlos:
        df=pd.DataFrame(out)
        df.index=sarr
        dffile=dir+'los/%s.%d/%s.%d.los.p' % (ds.step,nside,ds.id,i)
        df.to_pickle(dffile)

      nH=out['density']
      dtot=out['density'].sum()
      doTtot=(out['density']/out['temperature']).sum()

      map['column_density'].append(dtot*NHfact)
      map['temperature'].append(dtot/doTtot)
      map['v_r'].append((nH*out['velocity_Z']).sum()/dtot)
      map['v_th'].append((nH*out['velocity_X']).sum()/dtot)
      map['v_phi'].append((nH*out['velocity_Y']).sum()/dtot)

      if mhd: 
        Bperp2=out['magnetic_field_X']**2+out['magnetic_field_Y']**2
        B2=Bperp2+out['magnetic_field_Z']**2
        cos2phi=(out['magnetic_field_X']**2-out['magnetic_field_Y']**2)/Bperp2
        sin2phi=out['magnetic_field_X']*out['magnetic_field_Y']/Bperp2
        cosgam2=Bperp2/B2

        I=Bnu*(1-p0*(cosgam2-2./3.))*nH*dtau
        Q=p0*Bnu*cos2phi*cosgam2*nH*dtau
        U=p0*Bnu*sin2phi*cosgam2*nH*dtau
        map['Blos'].append((nH*out['magnetic_field_Z']).sum()/dtot)
        map['I'].append(I.sum())
        map['Q'].append(Q.sum())
        map['U'].append(U.sum())
    df=pd.DataFrame(map)
    dffile=dir+'los/%s.%s.map.%d.p' % (ds.id,ds.step,nside)
    df.to_pickle(dffile)


if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-n','--nside',type=int,default=32,help='Nside')
  parser.add_argument('-s','--serial',action='store_true',help='serial mode')
  parser.add_argument('-w','--write_los',action='store_true',help='write los data')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  args = parser.parse_args()
  main(**vars(args))
