#!python

import os
import re

def parse_filename(filename):
	"""
	#   PARSE_FILENAME    Break up a full-path filename into its component
	#   parts to check the extension, make it more readable, and extract the step
	#   number.  
	#
	#   PATH,BASENAME,STEP,EXT = ATH.PARSE_FILENAME(FILENAME)
	#
	#   E.g. If FILENAME='/home/Blast.0000.bin', then PATH='/home',
	#   BASENAME='Blast', STEP='0000', and EXT='bin'.
	#
	#   AUTHOR:  Chang-Goo Kim after ath_parse_filename.m by Aaron Skinner
	#   LAST MODIFIED:  11/19/2013
	"""

	path=os.path.dirname(filename)+os.path.sep
	base=os.path.basename(filename)
	split=re.split('\.',base)
	basename=split[0]
	step=int(split[1])
	ext=split[2]

	return path,basename,step,ext
