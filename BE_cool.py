from init import *

heat=2.e-26
def RK4(dydt,y0,t0,dt,Tc):
	dy1=dt*dydt(y0,t0,Tc)
	dy2=dt*dydt(y0+dy1/2.0,t0+dt/2.0,Tc)
	dy3=dt*dydt(y0+dy2/2.0,t0+dt/2.0,Tc)
	dy4=dt*dydt(y0+dy3,t0+dt,Tc)

	return y0+(1.0/6.0)*(dy1+2.0*dy2+2.0*dy3+dy4)



def cool(temp):
    c1, c2, t1, t2=1.e7, 1.4e-2, 1.184e5, 92.
    lamtmp = c1*np.exp(-t1/(temp+1.e3))+c2*np.sqrt(temp)*np.exp(-t2/temp)
    return lamtmp*2.e-26
    
def Teq(T0,P0):
	return 1.1*heat/cool(T0)*T0-P0

def deriv_cool(y,x,Tc):
	"""
	y[0] = P
	y[1] = y = dP/dx*x^2/rho
	"""
	nc = heat/cool(Tc)
	Pc = 1.1*nc*kbol*Tc
	Press = y[0]*Pc
	Temp = newton(Teq,10.,args=(Press/kbol,))
	rho = y[0]/Temp*Tc
	dPdx= y[1]/x**2*rho
	dydx= -rho*x**2
	return np.array([dPdx,dydx])

def deriv_iso(y,x,Tc):
	"""
	y[0] = P
	y[1] = y = dP/dx*x^2/rho
	"""
	dPdx= y[1]/x**2*y[0]
	dydx= -y[0]*x**2
	return np.array([dPdx,dydx])

class BEsolution:
	def __init__(self,Pedge):
		self.Pedge=Pedge
		self.r0=np.sqrt(1.1*kbol)/(1.4*mp)/np.sqrt(4*np.pi*Gconst)/pc

	def findsol(self,Tc,isoT=False):

		nc = heat/cool(Tc)
		Pc = 1.1*nc*kbol*Tc
		rc = self.r0*np.sqrt(Tc/nc)
		m0 = 4*np.pi*(rc*pc)**3*nc*1.4*mp/msun
		Pok = Pc/kbol

		dxi=1.e-3/rc
		y0=np.array([1.0,0.0])
		x0=dxi

		P=[]
		T=[]
		r=[]
		n=[]
		Mr=[]
		while 1:
			Pok=y0[0]*Pc/kbol
			if isoT:
		        	T0 = Tc
			else:
		        	T0 = newton(Teq,10.,args=(Pok,))
			P.append(Pok)
			T.append(T0)
			n.append(Pok/T0/1.1)
			r.append(x0*rc)
			Mr.append(-y0[1]*m0)
        
			if Pok<self.Pedge:
				break

			if isoT:
				y1=RK4(deriv_iso,y0,x0,dxi,Tc)
			else:
				y1=RK4(deriv_cool,y0,x0,dxi,Tc)
			x1=x0+dxi
			y0=y1
			x0=x1
                	
		return np.array(r), np.array(n), np.array(T), np.array(P), np.array(Mr)

	def getall(self):

		self.data=[]
		self.dataiso=[]
		for Tc in range(10,60,5): 
			solution=self.findsol(Tc)
			if len(solution[0]): self.data.append(solution)
		for Tc in range(10,60,5): 
			solution=self.findsol(Tc,isoT=True)
			if len(solution[0]): self.dataiso.append(solution)

		return self.data,self.dataiso

def plotall(BEcool,fig,dim=True):

    axes=[]
    for i in range(1,5): axes.append(fig.add_subplot(2,2,i))
    units=(1,1,1,1)
    r0=1

    colors=plt.cm.gist_rainbow(np.linspace(0,1,len(BEcool.data)))
    for data, dataiso, color in zip(BEcool.data,BEcool.dataiso,colors):
        Tc = data[2][0]
        nc = heat/cool(Tc)
        Pc = 1.1*nc*Tc
        rc = BEcool.r0*np.sqrt(Tc/nc)
        m0 = 4*np.pi*(rc*pc)**3*nc*1.4*mp/msun
        if(not dim):
            units = (nc, Tc, Pc, m0)
            r0 = rc

        for ax, i, unit in zip(axes, range(1,5), units):
            ax.plot(data[0]/r0,data[i]/unit,color=color)
            ax.plot(dataiso[0]/r0,dataiso[i]/unit,ls='--',color=color)
            ax.set_xscale('log')
            ax.set_yscale('log')

    if(dim):
        axes[0].set_ylabel('$n$ [cm$^-3$]')
        axes[1].set_ylabel('$T$ [K]')
        axes[2].set_ylabel('$P/k$ [cm$^-3$ K]')
        axes[3].set_ylabel('$M(<r)$ [$M_\odot$]')
        plt.setp(axes,'xlabel','$r$ [pc]')
        plt.setp(axes,'xlim',(1.e-2,1.e1))
        axes[1].set_ylim(1.e1,1.e2)
        axes[3].set_ylim(1.e-3,1.e3)
    else:
        axes[0].set_ylabel('$n/n_c$')
        axes[1].set_ylabel('$T/T_c$')
        axes[2].set_ylabel('$P/P_c$')
        axes[3].set_ylabel('$M(<r)/M_0$')
        plt.setp(axes,'xlabel','$r/r_0$')
        plt.setp(axes,'xlim',(1.e-1,1.e2))
        axes[3].set_ylim(1.e-3,1.e3)
        axes[1].set_ylim(1.e0,1.e1)
    mystyle()
    for ax in axes: plt.setp(ax.get_lines(),'linewidth',1)
    plt.tight_layout()
    
def plot_Pext(BEcool,fig,tau=False):

    ax=fig.add_subplot(111)
    colors=plt.cm.gist_rainbow(np.linspace(0,1,len(BEcool.data)))
    for data, dataiso, color in zip(BEcool.data,BEcool.dataiso,colors):
        Tc = data[2][0]
        nc = data[1][0]
        Pc = data[3][0]
        rc = BEcool.r0*np.sqrt(data[2][0]/data[1][0])
        #re = rc*(dataiso[2]/Tc)*(nc/dataiso[1]))
        m0 = 4*np.pi*(rc*pc)**3*nc*1.4*mp/msun
	if(tau): f=(Tc/data[2])**2
	else: f=1
        ax.plot(data[0]/rc,np.sqrt(data[3]/Pc)*(data[4]/m0)*f,color=color,label=str(Tc))
        ax.plot(dataiso[0]/rc,np.sqrt(dataiso[3]/Pc)*(dataiso[4]/m0),ls='--',color=color)

    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.axvline(6.45,ls=':')
    ax.axhline(1.18*np.sqrt(4*np.pi),ls=':')
    ax.set_xlabel(r'$\xi$')
    ax.set_ylim(1.e-1,1.e2)
    ax.set_xlim(1.e-1,1.e2)
    if(tau): ax.set_ylabel(r'$m \sqrt{\Pi}{\tau}^2$')
    else:  ax.set_ylabel(r'$m \sqrt{\Pi}$')
    mystyle()

def plot_MP(BEcool,fig,iso=False):
	Mtot=[]
	Pc=[]
	Pext=range(2000,5001,500)
	if iso: dataset=BEcool.dataiso
	else: dataset=BEcool.data
	for data in dataset:
		Tc = data[2][0]
        	nc = heat/cool(Tc)
        	rc = BEcool.r0*np.sqrt(Tc/nc)
        	mc = 4*np.pi*(rc*pc)**3*nc*1.4*mp/msun

    		MPext=interp1d(data[3][::-1],data[4][::-1])
    		TPext=interp1d(data[3][::-1],data[2][::-1])
    		for P in Pext: 
		       	if(iso): T0 = Tc
			else: T0 = newton(Teq,10.,args=(P,))
        		n0 = P/1.1/T0
        		r0 = BEcool.r0*np.sqrt(T0/n0)
        		m0 = 4*np.pi*(r0*pc)**3*n0*1.4*mp/msun
			#if(not iso): m0 = 1
        		if P>data[3].max(): Mtot.append(0.)
        		else: Mtot.append(MPext(P)/m0)
    			Pc.append(data[3][0]/P)

	print len(Pc), len(Mtot)
	Mtot=np.array(Mtot)
	Pc=np.array(Pc)
	Mtot=Mtot.reshape(len(BEcool.data),len(Pext))
	Pc=Pc.reshape(len(BEcool.data),len(Pext))

	fig.clf()
	ax=fig.add_subplot(111)
	lines=ax.plot(Pc,Mtot,marker='o')
	#lines=ax.plot(Tc,Mtot,marker='o')
	for line,i in zip(lines,range(len(Pext))): line.set_label(str(Pext[i]))
	ax.set_xlabel('$P_c/P_{ext}$')
	ax.set_ylabel('$M_{tot}/M_{0,e}$')
	if(iso): ax.axhline(np.sqrt(4*np.pi)*1.18,ls=':')
	ax.set_xscale('log')
	mystyle()
	leg=ax.legend()
