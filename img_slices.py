#!/usr/bin/python
from init import *

def display_slices(grid,x1,x2,n2d,P2d,T2d,tail='-XY',cbar=False):
	interp='quadric'
	interp='bilinear'
	interp='nearest'
	xyext=(x1.min(),x1.max(),x2.min(),x2.max())	
	slices=(n2d,P2d,T2d)
	labels=('n'+tail,'P'+tail,'T'+tail)
	log=False
	if T2d.max() < 10: log=True
	if log:
		mins=(-1,3,1)
		maxs=(5,5,4)
	else:
		mins=(0,2000,0)
		maxs=(100,4000,8000)

	from matplotlib.colors import LogNorm
	for ax, data, label, dmin, dmax in zip(grid, slices, labels, mins, maxs ):
		im=ax.imshow(data, interpolation=interp, extent=xyext, origin='lower',
			cmap=cm.coolwarm,
			vmin=dmin,vmax=dmax)
		if log:
			ticks=np.linspace(dmin,dmax,dmax-dmin+1)
		else:
			ticks=(dmin,0.5*(dmin+dmax),dmax)
		cb=ax.cax.colorbar(im,ticks=ticks)
		ax.cax.toggle_label(cbar)

def img_slices(fig,TIdata,log=True,vel=True,zoom=1,vskip=1,**kwargs):

	fig.clear()
	if kwargs.has_key('step'):
		if kwargs['step'] != TIdata.step: TIdata.reset_time(kwargs['step'])
	if log: 
		TIdata.convert_log()
		lprefix = 'log '
	else: 
		TIdata.convert_lin()
		lprefix = ''	
	n=TIdata.n
	P=TIdata.P
	Temp=TIdata.T
	x=TIdata.x1
	y=TIdata.x2
	z=TIdata.x3

	v1=TIdata.v1
	v2=TIdata.v2
	v3=TIdata.v3
	

	ndim = n.ndim
	if n.ndim == 3 and n.shape[-1] == 1: ndim=2

	if kwargs.has_key('xind'):
		xind = kwargs['xind']
	else:
		xind = x.size/2

	if kwargs.has_key('yind'):
		yind = kwargs['yind']
	else:
		yind = y.size/2
	if ndim == 3:
		if kwargs.has_key('zind'):
			zind = kwargs['zind']
		else:
			zind = z.size/2
		nxy = n[zind,:,:]
		Pxy = P[zind,:,:]
		Txy = Temp[zind,:,:]
	else:
		nxy = n.reshape(y.size,x.size)
		Pxy = P.reshape(y.size,x.size)
		Txy = Temp.reshape(y.size,x.size)

	grid = axes_grid.ImageGrid(fig,131,
			 nrows_ncols = (3,1),
			 label_mode = "L",
			 direction='row',
			 share_all = True,
			 cbar_mode="each",
			 axes_pad=0.10,
			)

        display_slices(grid,x,y,nxy,Pxy,Txy,tail='-XY')
	if vel:
		q=grid[0].quiver(x[::vskip],y[::vskip],
                    v1[zind,::vskip,::vskip],v2[zind,::vskip,::vskip],
                    pivot='tip',color='g',scale=x.size/2/vskip,
	            headwidth=10,headlength=10,headaxislength=10)
		qk = grid[0].quiverkey(q, 0.8, 1.05, 1, r'$1 km/s$',
          	    labelpos='E', fontproperties={'weight': 'bold'})

	grid[0].set_title('XY')
	grid[0].set_ylabel(lprefix+'n')
	grid[1].set_ylabel(lprefix+'P')
	grid[2].set_ylabel(lprefix+'T')
	grid[0].set_xlim(np.array(grid[0].get_xlim())/zoom)
	grid[0].set_ylim(np.array(grid[0].get_ylim())/zoom)


	if ndim == 3:

		nxz = n[:,yind,:]
		Pxz = P[:,yind,:]
		Txz = Temp[:,yind,:]
		nyz = n[:,:,xind]
		Pyz = P[:,:,xind]
		Tyz = Temp[:,:,xind]
	
		grid = axes_grid.ImageGrid(fig,132,
		                nrows_ncols = (3,1),
				direction='row',
				label_mode = "1",
				share_all = True,
				cbar_mode="each",
			 	axes_pad=0.10,
				)

        	display_slices(grid,x,z,nxz,Pxz,Txz,tail='-XZ')
		grid[0].set_title('XZ')
		grid[2].set_yticklabels('')
		grid[0].set_xlim(np.array(grid[0].get_xlim())/zoom)
		grid[0].set_ylim(np.array(grid[0].get_ylim())/zoom)

		grid = axes_grid.ImageGrid(fig,133,
		                nrows_ncols = (3,1),
				direction='row',
				label_mode = "1",
				share_all = True,
				cbar_mode="each",
			 	axes_pad=0.10,
				)

        	display_slices(grid,y,z,nyz,Pyz,Tyz,tail='-YZ',cbar=True)
		grid[0].set_title('YZ')
		grid[2].set_yticklabels('')
		grid[0].set_xlim(np.array(grid[0].get_xlim())/zoom)
		grid[0].set_ylim(np.array(grid[0].get_ylim())/zoom)

	plt.draw()

def img_slices_n(fig,TIdata,log=True,vel=True,zoom=1,vskip=1,**kwargs):

	fig.clear()
	if kwargs.has_key('step'):
		if kwargs['step'] != TIdata.step: TIdata.reset_time(kwargs['step'])
	if log: 
		TIdata.convert_log()
		lprefix = 'log '
	else: 
		TIdata.convert_lin()
		lprefix = ''	
	n=TIdata.n
	P=TIdata.P
	Temp=TIdata.T
	x=TIdata.x1
	y=TIdata.x2
	z=TIdata.x3

	v1=TIdata.v1
	v2=TIdata.v2
	v3=TIdata.v3
	

	ndim = n.ndim
	if n.ndim == 3 and n.shape[-1] == 1: ndim=2

	if kwargs.has_key('xind'):
		xind = kwargs['xind']
	else:
		xind = x.size/2

	if kwargs.has_key('yind'):
		yind = kwargs['yind']
	else:
		yind = y.size/2
	if ndim == 3:
		if kwargs.has_key('zind'):
			zind = kwargs['zind']
		else:
			zind = z.size/2
		nxy = n[zind,:,:]
		Pxy = P[zind,:,:]
		Txy = Temp[zind,:,:]
	else:
		nxy = n.reshape(y.size,x.size)
		Pxy = P.reshape(y.size,x.size)
		Txy = Temp.reshape(y.size,x.size)

	grid = axes_grid.ImageGrid(fig,131,
			 nrows_ncols = (3,1),
			 label_mode = "L",
			 direction='row',
			 share_all = True,
			 cbar_mode="each",
			 axes_pad=0.10,
			)

        display_slices(grid,x,y,nxy,Pxy,Txy,tail='-XY')
	if vel:
		q=grid[0].quiver(x[::vskip],y[::vskip],
                    v1[zind,::vskip,::vskip],v2[zind,::vskip,::vskip],
                    pivot='tip',color='g',scale=x.size/2/vskip,
	            headwidth=10,headlength=10,headaxislength=10)
		qk = grid[0].quiverkey(q, 0.8, 1.05, 1, r'$1 km/s$',
          	    labelpos='E', fontproperties={'weight': 'bold'})

	grid[0].set_title('XY')
	grid[0].set_ylabel(lprefix+'n')
	grid[1].set_ylabel(lprefix+'P')
	grid[2].set_ylabel(lprefix+'T')
	grid[0].set_xlim(np.array(grid[0].get_xlim())/zoom)
	grid[0].set_ylim(np.array(grid[0].get_ylim())/zoom)


	if ndim == 3:

		nxz = n[:,yind,:]
		Pxz = P[:,yind,:]
		Txz = Temp[:,yind,:]
		nyz = n[:,:,xind]
		Pyz = P[:,:,xind]
		Tyz = Temp[:,:,xind]
	
		grid = axes_grid.ImageGrid(fig,132,
		                nrows_ncols = (3,1),
				direction='row',
				label_mode = "1",
				share_all = True,
				cbar_mode="each",
			 	axes_pad=0.10,
				)

        	display_slices(grid,x,z,nxz,Pxz,Txz,tail='-XZ')
		grid[0].set_title('XZ')
		grid[2].set_yticklabels('')
		grid[0].set_xlim(np.array(grid[0].get_xlim())/zoom)
		grid[0].set_ylim(np.array(grid[0].get_ylim())/zoom)

		grid = axes_grid.ImageGrid(fig,133,
		                nrows_ncols = (3,1),
				direction='row',
				label_mode = "1",
				share_all = True,
				cbar_mode="each",
			 	axes_pad=0.10,
				)

        	display_slices(grid,y,z,nyz,Pyz,Tyz,tail='-YZ',cbar=True)
		grid[0].set_title('YZ')
		grid[2].set_yticklabels('')
		grid[0].set_xlim(np.array(grid[0].get_xlim())/zoom)
		grid[0].set_ylim(np.array(grid[0].get_ylim())/zoom)

	plt.draw()

