#!python

import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.axes_grid1.axes_grid as axes_grid
import mpl_toolkits.axes_grid1.inset_locator as inset_axes

def ath_plot_1d(data):
	x=data.grid['x1']
	fig = plt.figure()
	ax1 = fig.add_subplot(221)
	ax1.plot(x,data.d)
	ax2 = fig.add_subplot(222)
	ax2.plot(x,data.v1)
	ax3 = fig.add_subplot(223)
	ax3.plot(x,data.v2)
	ax4 = fig.add_subplot(224)
	ax4.plot(x,data.v3)
		
def ath_plot_2d(fig,data):

	grid = axes_grid.ImageGrid(fig, 111,
	                           nrows_ncols=(3,3),
	                           ngrids=None,
	                           direction='row',
	                           axes_pad=0.1, add_all=True,
	                           share_all=True, aspect=True,
	                           cbar_mode='each',
	                           cbar_location='top',
	                           cbar_size='7%',
	                           cbar_pad='1%')

	i=0
	for imdata in data:
		ax = grid[i]
#		cax = inset_axes(ax, width="100%",height = "8%", loc=3,
#	                         bbox_to_anchor = (1.05,0,1,1),
##	                         bbox_transform=ax.transAxes,
#		                 borderpad=0.
#		                 )
		shape=imdata.shape
		if imdata.ndim > 2: imdata = imdata[:,:,shape[2]/2]
		im = ax.imshow(imdata,origin="lower")
		ax.cax.colorbar(im)
		i=i+1
	plt.draw()

	return grid

def ath_slice_2d(fig,imdata):

	grid = axes_grid.ImageGrid(fig, 111,
	                           nrows_ncols=(1,3),
	                           ngrids=None,
	                           direction='row',
	                           axes_pad=0.1, add_all=True,
	                           share_all=True, aspect=True,
	                           cbar_mode='each',
	                           cbar_location='top',
	                           cbar_size='7%',
	                           cbar_pad='1%')

	for i in range(3):
		ax = grid[i]
#		cax = inset_axes(ax, width="100%",height = "8%", loc=3,
#	                         bbox_to_anchor = (1.05,0,1,1),
##	                         bbox_transform=ax.transAxes,
#		                 borderpad=0.
#		                 )
		shape=imdata.shape
		if imdata.ndim > 2: 
			xydata = imdata[:,:,shape[2]/2]
			xzdata = imdata[:,shape[1]/2,:]
			yzdata = imdata[shape[0]/2,:,:]
		im = ax.imshow(imdata,origin="lower")
		ax.cax.colorbar(im)
		i=i+1
	plt.draw()

	return grid



# usage:
#fig = plt.figure(1, figsize=(10,10), dpi=70)
#fdata = ath_data(filename)
#ath_plot_2d(fig,fdata.data,fdata.grid)

