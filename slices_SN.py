from pyathena import *
import glob
import os
import argparse
import cPickle as pickle

# matplotlib
import matplotlib
matplotlib.use('agg')
from mpl_toolkits.axes_grid1 import AxesGrid,ImageGrid
import matplotlib.backends.backend_agg
import matplotlib.figure 
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import rc

# astropy
import astropy.constants as c
import astropy.units as u

import color_maps as cm

def texteffect():
  try:
    from matplotlib.patheffects import withStroke
    myeffect = withStroke(foreground="w", linewidth=3)
    kwargs = dict(path_effects=[myeffect], fontsize=15)
  except ImportError:
    kwargs = dict(fontsize=15)
  return kwargs

def draw_slice(slice,ax):
  im=ax.imshow(slice.data,origin='lower',interpolation='nearest')
  im.set_extent(slice.bound)
  ax.set_xlabel(slice.axis_labels[0]+' [pc]')
  ax.set_ylabel(slice.axis_labels[1]+' [pc]')

  return im

def get_aux():
  aux={}
  aux['density']={'title':r'$n_{\rm H}$','cmap':'jet','cmin':5.e-3,'cmax':5.e2,'factor':1.0,'scale':'log'}
  aux['surface_density']={'title':r'$\Sigma$','cmap':'Spectral_r','cmin':1,'cmax':100,'factor':1.0,'scale':'log'}
  aux['number_density']={'title':r'$n_{\rm H} [{\rm cm}^{-3}]$','cmap':'Spectral_r','cmin':1.e-5,'cmax':1.e1,'factor':1.0,'scale':'log'}
  aux['pressure']={'title':r'$P_{\rm th}/k_B$','cmap':'hot','cmin':5.e0,'cmax':5.e4,'factor':1.0/c.k_B.cgs,'scale':'log'}
  aux['magnetic_pressure']={'title':r'$P_{\rm mag}/k_B$','cmap':'hot','cmin':5.e0,'cmax':5.e4,'factor':1.0/c.k_B.cgs,'scale':'log'}
  aux['turb_magnetic_pressure']={'title':r'$\delta P_{\rm mag}/k_B$','cmap':'hot','cmin':5.e0,'cmax':5.e4,'factor':1.0/c.k_B.cgs,'scale':'log'}
  aux['plasma_beta']={'title':r'$\beta$','cmap':'bone','cmin':5.e-2,'cmax':5.e2,'factor':1.0,'scale':'log'}
  aux['velocity1']={'title':r'$v_x$','cmap':'RdBu_r','cmin':-10,'cmax':10,'factor':1.e-5,'scale':'linear'}
  aux['velocity2']={'title':r'$v_y$','cmap':'RdBu_r','cmin':-10,'cmax':10,'factor':1.e-5,'scale':'linear'}
  aux['velocity3']={'title':r'$v_z$','cmap':'RdBu_r','cmin':-10,'cmax':10,'factor':1.e-5,'scale':'linear'}
  aux['magnetic_energy3']={'title':r'$P_{\rm mag,z}/k_B$','cmap':'Purples_r','cmin':5.e0,'cmax':5.e4,'factor':2.0/c.k_B.cgs,'scale':'log'}
  aux['kinetic_energy3']={'title':r'$P_{\rm turb,z}/k_B$','cmap':'hot','cmin':5.e0,'cmax':5.e4,'factor':2.0/c.k_B.cgs,'scale':'log'}
  aux['vmag']={'title':r'$v_{\rm mag}$','cmap':'jet','cmin':1,'cmax':1000,'factor':1.0,'scale':'log'}
  aux['temperature']={'title':r'$T$ [K]','cmap':'RdYlBu_r','cmin':10,'cmax':1.e10,'factor':1.0,'scale':'log'}
  aux['sound_speed']={'title':r'$c_s$','cmap':'RdBu_r','cmin':1,'cmax':100,'factor':1.e-5,'scale':'log'}
  aux['alfven_velocity3']={'title':r'$v_{A,z}$','cmap':'RdBu_r','cmin':1,'cmax':100,'factor':1.e-5,'scale':'log'}

  return aux

def main(**kwargs):
  rc('text', usetex=True)
  rc('font', size=15)
  rc('font',family='serif')
  rc('font',weight='medium')
  rc('xtick',labelsize=15)
  rc('ytick',labelsize=15)

  Myr = (c.pc/u.km*u.s).to('Myr').value
  dir=kwargs['base_directory']+kwargs['directory']
  id=kwargs['id']
  fields=kwargs['fields']
  file = glob.glob(dir+"id0/"+id+".????.vtk")
  file.sort()
  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
  else:
    start = 0
    end = len(file)
    fskip = 1

  new_mode=kwargs['new']

  ysize=15
  xsize=5*len(fields)
  file=file[start:end:fskip]
  fig = matplotlib.figure.Figure(figsize=(xsize,ysize))

  aux = get_aux()
  units = {}

  if not os.path.isdir(dir+'png/'): os.mkdir(dir+'png/')
  if not os.path.isdir(dir+'pickles/'): os.mkdir(dir+'pickles/')

  dspickle=dir+'pickles/'+id+'.0000.ds.p'
  ds=pickle.load(open(dspickle,'rb'))
  grids=ds.grids

  for f in file:
    print 'Reading: ',f
    ds = AthenaDataSet(f,grids=grids)
    for sltype in ['slice']:
      for axis in [1]:#range(3):
        ImGrid = ImageGrid(fig, 111,\
                           nrows_ncols = (1, len(fields)),\
                           direction = "row",\
                           axes_pad = 0.3,\
                           label_mode = "L",\
                           share_all = False,\
                           cbar_location="top",\
                           cbar_mode="each",\
                           cbar_size="3%",\
                           cbar_pad="10%")
  
        
        slicepickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,sltype,axis)
        slice = pickle.load(open(slicepickle,'rb'))
        for iax,field in enumerate(fields):
          ax=ImGrid[iax]
          if field=='turb_magnetic_pressure':
            dslice=slice['magnetic_pressure']
            units['turb_magnetic_pressure'] = 1.0*dslice.units.cgs
            if sltype == 'surf':
              Bpickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'Bturb',axis)
            elif sltype == 'slice':
              Bpickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'Bturbs',axis)
            Bturb = pickle.load(open(Bpickle,'rb'))
            dslice.data = Bturb[field]
          elif field=='vmag':
            dslice=slice['velocity1']
            units['vmag']=1.0*dslice.units.cgs
            dslice.data = np.sqrt(slice['velocity1'].data**2+slice['velocity2'].data**2+slice['velocity3'].data**2)
          else:
            dslice=slice[field]
          if not units.has_key(field): units[field]=1.0*dslice.units.cgs
          dslice.data *= units[field]
          dslice.data *= aux[field]['factor']

          im = draw_slice(dslice,ax)
          print field,dslice.data.min()
          faux=aux[field]
          if faux['cmin']>0: im.set_norm(LogNorm())
          im.set_cmap(plt.get_cmap(faux['cmap']))
          im.set_clim(faux['cmin'],faux['cmax'])
          cb=fig.colorbar(im,cax=ax.cax,orientation='horizontal')
          cb.set_label(faux['title'],**(texteffect()))
  
        ax=fig.axes[0]
        t1=ax.text(dslice.bound[0]*0.90,dslice.bound[3]*0.90,\
                   r'$t$=%5.2f Myr' % (ds.domain['time']*Myr),\
                   ha="left", va="top", **(texteffect()))
        pngfname=dir+'png/%s.%s.%s.%1d.png' % (ds.id,ds.step,sltype,axis)
        canvas = matplotlib.backends.backend_agg.FigureCanvasAgg(fig)
        canvas.print_figure(pngfname,bbox_inches='tight')
        fig.clf()
        print 'Output: ',pngfname

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('fields',type=str,nargs='*',
                      default=['density','pressure','magnetic_pressure','plasma_beta'],
                      help='fields')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  parser.add_argument('--new',action='store_true',default=False,
                      help='toggle to store pickle files')
  args = parser.parse_args()
  main(**vars(args))



