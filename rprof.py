#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import pyathena as pa
import get_los as gl
import glob
import os
import pandas as pd


def get_rprof(ds,theta,phi,field='momentum'):
  rhat=[np.sin(theta)*np.cos(phi),np.sin(theta)*np.sin(phi),np.cos(theta)]

  domain=ds.domain
  d=ds.read_all_data('density')
  v1=ds.read_all_data('velocity1')
  v2=ds.read_all_data('velocity2')
  v3=ds.read_all_data('velocity3')
  T=ds.read_all_data('temperature')
  idx,sarr=gl.los_idx(rhat,domain,smax=domain['right_edge'].max()*1.1,smin=domain['dx'][0])
  d=gl.interp3D(d,idx)
  v1=gl.interp3D(v1,idx)
  v2=gl.interp3D(v2,idx)
  v3=gl.interp3D(v3,idx)
  T=gl.interp3D(T,idx)
  vr=v1*rhat[0]+v2*rhat[1]+v3*rhat[2]
  p=1.1*d*T

  return sarr,d,p,T,vr


def main(read=False):
  dir='/scr1/cgkim/Research/athena_sntest/bin/'
  #ids=['SNEJ_3pc_multi_dt001','SNTH_3pc_multi_dt001','SNTH_3pc_multi_dt001_vr']
  ids=['SNEJ_2pc_multi','SNTH_2pc_multi','SNTH_3pc_multi_dt01_vr']
  fig,axes=plt.subplots(2,2,**{'num':2})
  for ax in axes.flatten(): ax.cla()
  c=['b','g','r']
  l=[]
  for i,id in enumerate(ids):
    files=glob.glob(dir+'id0/'+id+'.*.vtk')
    files.sort()
    nf=len(files)
  
    for f in files[::100]:
      file_rprof=f+'.rprof'
      if not os.path.isfile(file_rprof) or read:
        ds=pa.AthenaDataSet(f)
        r,d,P,T,vr=get_rprof(ds,np.pi/4,np.pi/4)
        df=pd.DataFrame({'d':d,'P':P,'T':T,'vr':vr},index=r)
        df.to_pickle(f+'.rprof')
      df = pd.read_pickle(file_rprof)
      df.plot(subplots=True,ax=axes,legend=False,color=c[i])
    l.append(plt.gca().lines[-1])

  axes=axes.flatten()
  plt.setp(axes[0:-1],'yscale','log')
  axes[0].legend(l,ids)
  for ax,yl in zip(axes,df.columns): ax.set_ylabel(yl)
    
  plt.tight_layout()
