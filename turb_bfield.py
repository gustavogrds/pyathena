from pyathena import *
import glob
import os
import argparse
import cPickle as pickle

# matplotlib
import matplotlib
matplotlib.use('agg')
from mpl_toolkits.axes_grid1 import AxesGrid,ImageGrid
import matplotlib.backends.backend_agg
import matplotlib.figure 
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import rc

# astropy
import astropy.constants as c
import astropy.units as u

# helicity
from derivative import *
def construct3d(array,shape,axis=0):
  if array.shape[0] != shape[axis]:
    print "Error: input array dimension (%d) should match with output dimension (%d) of axis %d" % (array.shape[0], shape[axis], axis)
    return -1
  else:
    if axis == 0:
      array_3d = np.tile(array.reshape(shape[0],1,1),(1,shape[1],shape[2]))
    elif axis == 1:
      array_3d = np.tile(array.reshape(1,shape[1],1),(shape[0],1,shape[2]))
    elif axis == 2:
      array_3d = np.tile(array.reshape(1,1,shape[2]),(shape[0],shape[1],shape[2]))
    return array_3d

def main(**kwargs):
  dir=kwargs['base_directory']+kwargs['directory']
  id=kwargs['id']
  if kwargs['serial']: 
    file = glob.glob(dir+id+".????.vtk")
  else:
    file = glob.glob(dir+"id0/"+id+".????.vtk")
  file.sort()
  if kwargs['range'] != '':
    sp=kwargs['range'].split(',')
    start = eval(sp[0])
    end = eval(sp[1])
    fskip = eval(sp[2])
  else:
    start = 0
    end = len(file)
    fskip = 1


  file=file[start:end:fskip]

  if not os.path.isdir(dir+'png/'): os.mkdir(dir+'png/')
  if not os.path.isdir(dir+'pickles/'): os.mkdir(dir+'pickles/')

  dspickle=dir+'pickles/'+id+'.0000.ds.p'
  ds=pickle.load(open(dspickle,'rb'))
  grids=ds.grids

  dm=ds.domain
  le=dm['left_edge']+0.5*dm['dx']
  re=dm['right_edge']-0.5*dm['dx']
  Nx=dm['Nx']
  x=np.linspace(le[0],re[0],Nx[0])
  y=np.linspace(le[1],re[1],Nx[1])
  z=np.linspace(le[2],re[2],Nx[2])

  for f in file:
    print 'Reading: ',f
    ds = AthenaDataSet(f,grids=grids)
    Bpickle=dir+'pickles/'+'%s.%s.%s.p' % (ds.id,ds.step,'Bturb')
    hpickle=dir+'pickles/'+'%s.%s.%s.p' % (ds.id,ds.step,'helicity')

    h = {}
#    if os.path.isfile(Bpickle): Bturb=pickle.load(open(Bpickle,'rb'))
#    else:
    #den = ds.read_all_data('density')
    B1 = ds.read_all_data('magnetic_field1')
    B2 = ds.read_all_data('magnetic_field2')
    B3 = ds.read_all_data('magnetic_field3')

    B1mean = (B1.mean(axis=1)).mean(axis=1)
    B2mean = (B2.mean(axis=1)).mean(axis=1)
    B3mean = (B3.mean(axis=1)).mean(axis=1)

    #rhomean = (den.mean(axis=1)).mean(axis=1)
    #B1wmean = ((B1*den).mean(axis=1)).mean(axis=1)/rhomean
    #B2wmean = ((B2*den).mean(axis=1)).mean(axis=1)/rhomean
    #B3wmean = ((B3*den).mean(axis=1)).mean(axis=1)/rhomean

    B1turb = B1 - construct3d(B1mean,B1.shape)
    B2turb = B2 - construct3d(B2mean,B2.shape)
    B3turb = B3 - construct3d(B3mean,B3.shape)

    Bturb = {}
    Bturb['B1']=B1turb
    Bturb['B2']=B2turb
    Bturb['B3']=B3turb
    
#      pickle.dump(Bturb,open(Bpickle,'wb'),pickle.HIGHEST_PROTOCOL)

    B1turb=Bturb['B1']
    B2turb=Bturb['B2']
    B3turb=Bturb['B3']

    print 'total magnetic helicity:'
    HB=helicity(B1,B2,B3,x,y,z)
    print 'turbulent magnetic helicity:'
    HBturb=helicity(B1turb,B2turb,B3turb,x,y,z)
    h['total_magnetic']=HB
    h['turb_magnetic']=HBturb

    v1 = ds.read_all_data('velocity1')
    v2 = ds.read_all_data('velocity2')
    v3 = ds.read_all_data('velocity3')
    print 'turbulent kinetic helicity:'
    HV=helicity(v1,v2,v3,x,y,z)
    h['turb_kinetic']=HV

    pickle.dump(h,open(hpickle,'wb'),pickle.HIGHEST_PROTOCOL)

    s=B1.shape
    print s
    for axis in range(3):
# mean projection
      Bturb = {}
      Bturb['turb_magnetic_energy1']=(B1turb**2/(8*np.pi)).mean(axis=axis)
      Bturb['turb_magnetic_energy2']=(B2turb**2/(8*np.pi)).mean(axis=axis)
      Bturb['turb_magnetic_energy3']=(B3turb**2/(8*np.pi)).mean(axis=axis)
      Bturb['turb_magnetic_pressure']=((B1turb**2+B2turb**2+B3turb**2)/(8*np.pi)).mean(axis=axis)
      Bturb['turb_magnetic_stress']=((B1turb*B2turb)/(4*np.pi)).mean(axis=axis)
      Bpickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'Bturb',axis)
      pickle.dump(Bturb,open(Bpickle,'wb'),pickle.HIGHEST_PROTOCOL)

      h = {}
      h['total_magnetic']=(HB).mean(axis=axis)
      h['turb_magnetic']=(HBturb).mean(axis=axis)
      h['turb_kinetic']=(HV).mean(axis=axis)
      hpickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'helicity',axis)
      pickle.dump(h,open(hpickle,'wb'),pickle.HIGHEST_PROTOCOL)

# slice at (0,0,0)
      Bturb = {}
     
      if axis == 0:
        Bturb1=B1turb[s[axis]/2+1,:,:]
        Bturb2=B2turb[s[axis]/2+1,:,:]
        Bturb3=B3turb[s[axis]/2+1,:,:]
        HBs=HB[s[axis]/2+1,:,:]
        HBturbs=HBturb[s[axis]/2+1,:,:]
        HVs=HV[s[axis]/2+1,:,:]
      if axis == 1:
        Bturb1=B1turb[:,s[axis]/2+1,:]
        Bturb2=B2turb[:,s[axis]/2+1,:]
        Bturb3=B3turb[:,s[axis]/2+1,:]
        HBs=HB[:,s[axis]/2+1,:]
        HBturbs=HBturb[:,s[axis]/2+1,:]
        HVs=HV[:,s[axis]/2+1,:]
      if axis == 2:
        Bturb1=B1turb[:,:,s[axis]/2+1]
        Bturb2=B2turb[:,:,s[axis]/2+1]
        Bturb3=B3turb[:,:,s[axis]/2+1]
        HBs=HB[:,:,s[axis]/2+1]
        HBturbs=HBturb[:,:,s[axis]/2+1]
        HVs=HV[:,:,s[axis]/2+1]
      Bturb['turb_magnetic_energy1']=Bturb1**2/(8*np.pi)
      Bturb['turb_magnetic_energy2']=Bturb2**2/(8*np.pi)
      Bturb['turb_magnetic_energy3']=Bturb3**2/(8*np.pi)
      Bturb['turb_magnetic_pressure']=(Bturb1**2+Bturb2**2+Bturb3**2)/(8*np.pi)
      Bturb['turb_magnetic_stress']=(Bturb1*Bturb2)/(4*np.pi)
      Bpickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'Bturbs',axis)
      pickle.dump(Bturb,open(Bpickle,'wb'),pickle.HIGHEST_PROTOCOL)

      h = {}
      h['total_magnetic']=HBs
      h['turb_magnetic']=HBturbs
      h['turb_kinetic']=HVs
      hpickle=dir+'pickles/'+'%s.%s.%s.%1d.p' % (ds.id,ds.step,'helicitys',axis)
      pickle.dump(h,open(hpickle,'wb'),pickle.HIGHEST_PROTOCOL)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()

  parser.add_argument('-b','--base_directory',type=str,
                      default='/tigress/changgoo/',
                      help='base working directory')
  parser.add_argument('-d','--directory',type=str,default='',
                      help='working directory')
  parser.add_argument('-i','--id',type=str,
                      help='id of dataset')
  parser.add_argument('-s','--serial',action='store_true',help='serial mode')
  parser.add_argument('-r','--range',type=str,default='',
                      help='time range, start:end:skip')
  args = parser.parse_args()
  main(**vars(args))
